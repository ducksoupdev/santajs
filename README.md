# Secret Santa Generator

This project is a NodeJS Secret Santa Generator. It's free to use and you can sign-up at [http://santa.ducksoupdev.co.uk](http://santa.ducksoupdev.co.uk)

## Background

This project was created as a playground project for NodeJS and AngularJS 1. Its far from perfect but it's a work-in-progress!

### Build

Grunt is used as the build tool with the following modules:

* [Mocha](http://visionmedia.github.io/mocha)
* [Watch](https://github.com/gruntjs/grunt-contrib-watch)
* [Shell](https://github.com/sindresorhus/grunt-shell)
* [CSSMin](https://github.com/gruntjs/grunt-contrib-cssmin)
* [Uglify](https://github.com/gruntjs/grunt-contrib-uglify)
* [Sails-linker](https://github.com/Zolmeister/grunt-sails-linker)
* [Karma](https://github.com/karma-runner/karma)

### Back-end

The back-end is NodeJS with the following components:

* [Restify](http://mcavage.me/node-restify/)
* [Mongoose](http://mongoosejs.com/)
* [Mustache](http://mustache.github.com/)
* [Winston](https://github.com/flatiron/winston)
* [Underscore](http://underscorejs.org)
* [Nodemailer](https://github.com/andris9/Nodemailer)
* [Bunyan](https://github.com/trentm/node-bunyan)
* [Async](https://github.com/caolan/async)

### Testing

The back-end testing is done through [Mocha](http://visionmedia.github.io/mocha) and using [expectations](https://github.com/spmason/expectations) (Jasmine-style) tests. The integration tests use [Supertest](https://github.com/visionmedia/supertest).

Front-end testing is AngularJS Jasmine using [Karma](https://github.com/karma-runner/karma) test runner.

### Storage

Data is stored in [MongoDB](http://www.mongodb.org/) and [Mongoose](http://mongoosejs.com/) is used for object modeling

### Front-end

The front-end is built with [AngularJS](http://angularjs.org/) 1.3.0. It uses [ngStorage](https://github.com/gsklee/ngStorage) for localStorage of session data and [Underscore](http://underscorejs.org) as a utility library.

### Coverage

I have also added code coverage using [Istanbul](https://github.com/gotwarlost/istanbul) for both Mocha (back-end) and Karma (front-end).

## Running the project locally

### Dependencies

[Docker](https://www.docker.com/) is required for this project.

### How to run the project

Clone the repository

    $ git clone https://ducksoupdev@bitbucket.org/ducksoupdev/santajs.git

Run the project:

    $ docker-compose up

## Deployment

The project is currently deployed on a [Digital Ocean](https://www.digitalocean.com/) droplet.

### Production build

First, create a production build of the files:

    $ cd app && grunt build-prod

### Provision the droplet

#### Verify that `docker-machine` is installed:

    $ docker-machine -v

#### Create a [personal access token with write access](https://cloud.digitalocean.com/settings/api/tokens)

#### Use `docker-machine` to provision a host (droplet):

    $ docker-machine create HOST_NAME --driver=digitalocean --digitalocean-region=REGION --digitalocean-size=512mb --digitalocean-access-token=ACCESS_TOKEN

You can get the list of available regions by running:

    $ curl -X GET -H "Content-Type: application/json" -H "Authorization: Bearer ACCESS_TOKEN" "https://api.digitalocean.com/v2/regions"

#### Verify that the host has been created:

    $ docker-machine ls

#### Connect to the host:

    $ docker-machine env HOST_NAME
    $ eval $(docker-machine env HOST_NAME)

#### Run docker commands on the host:

    $ docker-machine env HOST_NAME
    $ docker-machine ip HOST_NAME
    $ docker-machine inspect HOST_NAME

#### Run compose to start the containers:

    $ docker-compose -f docker-compose-prod.yml up -d

