FROM node:6.9.1
MAINTAINER Matt Levy "ducksoupdev@gmail.com"
ENV REFRESHED_AT 2016_11_16

RUN npm install --global nodemon

COPY ./app /usr/src/app/
RUN rm -f -r /usr/src/app/node_modules
RUN mkdir -p /usr/src/app/logs
RUN useradd -U -ms /bin/bash app && chown -R app:app /usr/src/app

USER app
WORKDIR /usr/src/app/
RUN npm install --production

EXPOSE 8888

CMD ["npm", "start"]
