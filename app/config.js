(function() {
  /**
   * Created by levym on 16/11/13.
   */
  "use strict";
  module.exports = function() {
      switch(process.env.NODE_ENV) {
          case "test":
              return {
                  "url": {
                      "address": "http://localhost:8888"
                  },
                  "db": {
                      "mongodb": "mongodb://mongo/santajs_test"
                  },
                  "logger": {
                      "api": "logs/api_test.log",
                      "exception": "logs/exceptions_test.log",
                      "express": "logs/express_test.log"
                  },
                  "smtp": {
                      host: "mailcatcher", // docker container name
                      port: 1025,
                      debug: true
                  }
              };
          case "production":
              return {
                  "url": {
                      "address": "http://santa.ducksoupdev.co.uk"
                  },
                  "db": {
                      "mongodb": "mongodb://mongo/santajs"
                  },
                  "logger": {
                      "api": "logs/express.log",
                      "exception": "logs/exceptions.log",
                      "express": "logs/express.log"
                  },
                  "smtp": {
                      host: "smtp.sendgrid.net",
                      port: 587,
                      auth: {
                        user: "apikey",
                        pass: "xxx"
                      }
                  }
              };
          default:
              return {
                  "url": {
                      "address": "http://localhost:8888"
                  },
                  "db": {
                      "mongodb": "mongodb://mongo/santajs"
                  },
                  "logger": {
                      "api": "logs/express.log",
                      "exception": "logs/exceptions.log",
                      "express": "logs/express.log"
                  },
                  "smtp": {
                      host: "mailcatcher", // docker container name
                      port: 1025,
                      debug: true
                  }
              };
      }
  };
})();
