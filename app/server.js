(function() {
  /**
   * Created by levym on 18/11/13.
   */
  "use strict";

  var restify = require("restify"),
      bunyan = require("bunyan"),
      path = require("path"),
      Config = require("./config");

  // create a new config instance
  var config = new Config();

  // create a logger instance
  var log = bunyan.createLogger({
      name: "santajs",
      streams: [{
          path: config.logger.api
      }]
  });

  // restify server
  var server = restify.createServer({
    name: "santajs",
    log: log
  });

  // body parser for post requests
  server.use(restify.bodyParser({
    mapParams: true
  }));

  // routes
  var accountRoute = require("./routes/api/account");
  var personRoute = require("./routes/api/person");
  var routes = {
      account: accountRoute,
      person: personRoute
  };

  // account api
  server.get("/api/accountByShortUrl/:shortUrl", routes.account.get);
  server.get("/api/account/:id", routes.account.get);
  server.post("/api/account", routes.account.create);
  server.put("/api/account/:id", routes.account.update);
  server.post("/api/secret/:id", routes.account.verification);

  // person api
  server.get("/api/person/:id", routes.person.get);
  server.post("/api/person", routes.person.create);
  server.put("/api/person/:id", routes.person.update);
  server.del("/api/person/:id", routes.person.delete);
  server.get("/api/people/:accountId", routes.person.list);
  server.post("/api/auth", routes.person.authenticate);
  server.put("/api/auth", routes.person.autoLogin);
  server.get("/api/auth/:verificationCode", routes.person.verification);
  server.post("/api/allocate/:accountId", routes.person.allocate);

  // static
  var staticPath = path.join(__dirname, "public");
  server.get(/^\/?.*/, restify.serveStatic({
      directory: staticPath,
      default: "index.html"
  }));

  // start the application
  function start() {
      var port = process.env.PORT || 8888;
      server.listen(port, function() {
          console.log("Restify server listening on port %d in %s mode, static path %s", port, process.env.NODE_ENV, staticPath);
      });
  }

  module.exports.start = start;
})();
