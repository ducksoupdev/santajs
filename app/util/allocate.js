/**
 * Created by levym on 22/11/13.
 */
"use strict";

var __ = require("underscore")._;

var Allocate = {

    fromList: function(docs, reqName) {
        var allPeopleOk = [];
        var allocatedPersons = [];
        var currentPerson = null;

        // remove this person from allPeople
        __.each(docs, function(e, i) {
            if (e.allocatedPerson) {
                allocatedPersons.push(e.allocatedPerson);
            }
            if (e.name.toLowerCase() === reqName.toLowerCase()) {
                currentPerson = e;
            } else {
                allPeopleOk.push(e);
            }
        });

        if (currentPerson) {
            if (currentPerson.allocatedPerson) {
                return currentPerson.allocatedPerson;
            } else {
                var availPersons = [];
                var thisPerson = null;

                if (allPeopleOk.length > 0) {
                    __.each(allPeopleOk, function(e, i) {
                        var ap = __.findWhere(allocatedPersons, { name: e.name });
                        if (!ap) {
                            availPersons.push(e);
                        }
                    });

                    if (availPersons.length > 0) {
                        thisPerson = availPersons[__.random(0, availPersons.length - 1)];
                    }
                } else {
                    thisPerson = allPeopleOk[__.random(0, allPeopleOk.length - 1)];
                }

                if (thisPerson) {
                    return thisPerson;
                } else {
                    return null;
                }
            }
        } else {
            return null;
        }
    }

};

module.exports = Allocate;