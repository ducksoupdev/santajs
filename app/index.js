(function() {
  /**
   * Created by levym on 18/11/13.
   */
  "use strict";

  var winston = require("winston"),
      mongoose = require("mongoose"),
      server = require("./server"),
      Config = require("./config");

  // create a new config instance
  var config = new Config();

  // We will log normal api operations into api.log
  console.log("starting logger...");
  winston.add(winston.transports.File, {
      filename: config.logger.api,
      maxsize: 10000000,
      json: false,
      handleExceptions: true
  });

  console.log("logger started. Connecting to MongoDB...");
  mongoose.connect(config.db.mongodb);

  var db = mongoose.connection;
  db.on("error", console.error.bind(console, "connection error: "));
  db.on("open", function() {
      console.log("DB Opened");

      console.log("Successfully connected to MongoDB. Starting web server...");
      server.start();

      console.log("Successfully started web server. Waiting for incoming connections...");
  });
})();
