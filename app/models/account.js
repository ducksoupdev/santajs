(function() {
  /**
   * Created by levym on 16/11/13.
   */
  "use strict";

  var Account = function() {
      var mongoose = require("mongoose");
      var Schema = mongoose.Schema;

      // create the schema
      var _schema = new Schema({
          name: { type: String, required: true },
          shortUrl: String,
          secretWord: String,
          status: { type: String, default: "Active" },
          dateCreated: { type: Date, required: true },
          lastUpdated: { type: Date, required: true },
          adminPerson: {
              type: String,
              ref: "Person"
          }
      });

      // create the model
      var _model = mongoose.model("Account", _schema);

      // add private methods
      var _save = function(obj, success, fail) {
          if (obj._id) {
              _model.findOne({ "_id": obj._id }, function(err, doc) {
                  if (err) {
                      fail(err);
                  } else {
                      for (var prop in obj) {
                          if (obj.hasOwnProperty(prop)) {
                              doc[prop] = obj[prop];
                          }
                      }

                      // add the lastUpdated property
                      if (!doc["lastUpdated"]) {
                          doc.lastUpdated = new Date();
                      }

                      doc.save(function(err) {
                          if (err) {
                              fail(err);
                          } else {
                              success(doc);
                          }
                      });
                  }
              });
          } else {
              // create
              if (!obj["dateCreated"]) {
                  obj.dateCreated = new Date();
                  obj.lastUpdated = new Date();
              }
              _model.create(obj, function(err, doc) {
                  if (err) {
                      fail(err);
                  } else {
                      success(doc);
                  }
              });
          }
      };

      var _delete = function(id, success, fail) {
          _model.remove({ _id: id }, function(err) {
              if (err) {
                  fail(err);
              } else {
                  success();
              }
          });
      };

      var _exists = function(obj, success, fail) {
          _model.findOne(obj, function(err, doc) {
              if (err) {
                  fail(err);
              } else {
                  if (doc) {
                      success(true);
                  } else {
                      success(false);
                  }
              }
          });
      };

      var _find = function(obj, options, success, fail) {
          _model
              .find(obj, null, options)
              .populate("adminPerson")
              .exec(function(err, docs) {
                  if (err) {
                      fail(err);
                  } else {
                      success(docs);
                  }
              });
      };

      var _get = function(id, success, fail) {
          if (!_isValidId(id)) {
              fail(new Error("Invalid account ID"));
          } else {
              _model
                  .findOne({ _id: id })
                  .populate("adminPerson")
                  .exec(function(err, doc) {
                      if (err) {
                          fail(err);
                      } else {
                          success(doc);
                      }
                  });
          }
      };

      var _isValidId = function(id) {
          return /^[0-9a-fA-F]{24}$/.test(id);
      };

      // return an object with public methods
      return {
          name: "Account",
          schema: _schema,
          model: _model,
          save: _save,
          delete: _delete,
          exists: _exists,
          find: _find,
          get: _get
      };
  }();

  module.exports = Account;
})();
