(function() {
	"use strict";

	angular.module("santaApp")
			.controller("verificationController", verificationController);

	verificationController.$inject = [
		"$scope", 
		"$routeParams", 
		"$log", 
		"loginVerifyService",
		"flashService"
	];

    function verificationController(
    	$scope, 
    	$routeParams, 
    	$log, 
    	loginVerifyService,
    	flashService
	) {
		if ($routeParams.verificationCode) {
            $scope.login = loginVerifyService.get({ code: $routeParams.verificationCode }, function(data, headers) {
                var msg = "Your account has been successfully verified and activated! You can now log in and get started.";
                if (data.message) {
                    msg = data.message;
                }
                flashService.add("success", msg);
                $scope.displayLogin = true;
            }, function(response) {
                var message = "An unexpected error occurred";
                if (response.status === 404) {
                    message = response.data.message;
                    $log.error("404: " + message);
                } else if (response.status === 500) {
                    // a nodejs specific error object containing a message and stack
                    if (response.data.error) {
                        message = response.data.error.message;
                        $log.error("500: " + message + ", stack: " + response.data.error.stack);
                    } else {
                        message = response.data.message;
                        $log.error("500: " + message);
                    }
                } else {
                    $log.error(message);
                }
                flashService.add("error", message);
            });
        } else {
            flashService.add("error", "No verification code supplied!");
        }
	}
})();
