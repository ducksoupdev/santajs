(function() {
    "use strict";

    angular.module("santaApp")
        .controller("signupController", signupController);

    signupController.$inject = [
        "$scope",
        "$log",
        "accountService",
        "flashService"
    ];

    function signupController(
        $scope,
        $log,
        accountService,
        flashService
    ){
        var ctrl = this;

        $scope.focusInput = true;
        $scope.account = new accountService();
        $scope.formSubmitted = false;

        ctrl.signMeUp = function() {
            accountService.save($scope.account, function(response) {
                // success
                flashService.add("success", "Thanks for signing up! To complete the sign-up, We have emailed you an activation link. Please click the link in the email message to activate your account");
                $scope.formSubmitted = true;
            }, function(response) {
                var message = "An unexpected error occurred";
                if (response.status === 404) {
                    message = response.data.message;
                    $log.error("404: " + message);
                } else if (response.status === 500) {
                    // a nodejs specific error object containing a message and stack
                    if (response.data.error) {
                        message = response.data.error.message;
                        $log.error("500: " + message + ", stack: " + response.data.error.stack);
                    } else {
                        message = response.data.message;
                        $log.error("500: " + message);
                    }
                } else {
                    $log.error(message);
                }
                flashService.add("error", message);
            });
        };
    }
})();