(function() {
    describe("configure", function() {
        beforeEach(module("santaApp"));

        var location, route, rootScope;

        beforeEach(inject(function(_$location_, _$route_, _$rootScope_) {
            location = _$location_;
            route = _$route_;
            rootScope = _$rootScope_;
        }));

        describe("The /home route", function() {
            beforeEach(inject(
                function($httpBackend) {
                    $httpBackend.expectGET("js/app/home/home.html")
                        .respond(200);
                }));

            it("should load the home page on successful load of /home", function() {
                location.path("/home");
                rootScope.$digest();
                expect(route.current.controller).toBe("homeController");
            });
        });
    });
})();