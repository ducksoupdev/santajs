(function() {
    "use strict";

    angular.module("santaApp")
        .directive("focusMe", focusMe);

    focusMe.$inject = [
    	"$timeout"
    ];

    function focusMe(
    	$timeout
    ) {
        return function(scope, element, attrs) {
            scope.$watch(attrs.focusMe, function(value) {
                if (value) {
                    $timeout(function() {
                        element[0].focus();
                    }, 700);
                }
            });
        };
    }
})();
		