(function() {
    "use strict";

    angular.module("santaApp")
        .config(configBlock);

    configBlock.$inject = [
        "$routeProvider"
    ];

    function configBlock(
        $routeProvider
    ) {
        $routeProvider.when("/home", { templateUrl: "js/app/home/home.html", controller: "homeController", controllerAs: "ctrl" });
        $routeProvider.when("/signUp", { templateUrl: "js/app/signup/signUp.html", controller: "signupController", controllerAs: "ctrl" });
        $routeProvider.when("/login", { templateUrl: "js/app/login/login.html", controller: "loginController", controllerAs: "ctrl" });
        $routeProvider.when("/admin", { templateUrl: "js/app/admin/admin.html", controller: "adminController", controllerAs: "ctrl" });
        $routeProvider.when("/s/:shortUrl", { templateUrl: "js/app/home/santa.html", controller: "santaController", controllerAs: "ctrl" });
        $routeProvider.when("/v/:verificationCode", { templateUrl: "js/app/signup/verify.html", controller: "verificationController", controllerAs: "ctrl" });
        $routeProvider.otherwise({ redirectTo: "/home" });
    }
})();