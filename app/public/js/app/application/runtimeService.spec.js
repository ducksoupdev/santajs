(function () {
    "use strict";

    describe("runtimeService", function() {
        var $location, runtimeService;

        beforeEach(module("santaApp"));

        beforeEach(inject(function(_$location_, _runtimeService_) {
            $location = _$location_;
            runtimeService = _runtimeService_;
        }));

        describe("the useLiveData method in file mode", function() {
            beforeEach(function() {
                spyOn($location, "port").andCallFake(function() {
                    return 80;
                });
                spyOn($location, "protocol").andCallFake(function() {
                    return "file";
                });
            });

            it("Should return false", function() {
                expect(runtimeService.useLiveData()).toBeFalsy();
            });
        });

        describe("the useLiveData method in http mode when port >= 59000 and port <= 59999", function() {
            beforeEach(function() {
                spyOn($location, "port").andCallFake(function() {
                    return 59001;
                });
                spyOn($location, "protocol").andCallFake(function() {
                    return "http";
                });
            });

            it("Should return false", function() {
                expect(runtimeService.useLiveData()).toBeFalsy();
            });
        });

        describe("the useLiveData method in http mode when port >= 63000 && port <= 63999", function() {
            beforeEach(function() {
                spyOn($location, "port").andCallFake(function() {
                    return 63001;
                });
                spyOn($location, "protocol").andCallFake(function() {
                    return "http";
                });
            });

            it("Should return false", function() {
                expect(runtimeService.useLiveData()).toBeFalsy();
            });
        });

        describe("the useLiveData method in http mode", function() {
            beforeEach(function() {
                spyOn($location, "port").andCallFake(function() {
                    return 80;
                });
                spyOn($location, "protocol").andCallFake(function() {
                    return "http";
                });
            });

            it("Should return false", function() {
                expect(runtimeService.useLiveData()).toBeTruthy();
            });
        });
    });
})();
