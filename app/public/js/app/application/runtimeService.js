(function() {
    "use strict";

    angular.module("santaApp")
        .factory("runtimeService", runtimeService);

    runtimeService.$inject = [
        "$location"
    ];

    function runtimeService($location) {
        function isFileBased() {
            return $location.protocol() === "file";
        }

        return {
            useLiveData: function () {
                if (isFileBased()) {
                    return false;
                }

                var port = $location.port();
                if ((port >= 59000 && port <= 59999) || (port >= 63000 && port <= 63999)) {
                    return false;
                }

                return true;
            }
        };
    }
})();