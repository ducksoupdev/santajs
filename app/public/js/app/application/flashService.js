(function() {
    "use strict";

    angular.module("santaApp")
        .factory("flashService", flashService);

    flashService.$inject = [
    	"$rootScope"
    ];

    function flashService(
    	$rootScope
    ) {
        var flashService = {};

        // create an array of alerts available globally
        var messages = [];

        $rootScope.getMessages = function() {
            var tm = messages.slice(0);
            return tm;
        };

        flashService.add = function(level, text) {
            messages.push({ "level": level, "text": text });
        };

        flashService.clear = function() {
            messages = [];
        };

        $rootScope.$on("$routeChangeSuccess", flashService.clear);

        return flashService;
    }
})();
		