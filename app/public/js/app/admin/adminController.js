(function() {
    "use strict";

    angular.module("santaApp")
        .controller("adminController", adminController);

    adminController.$inject = [
        "$rootScope",
        "$scope",
        "$window",
        "$log",
        "$location",
        "loginService",
        "peopleService",
        "personService",
        "flashService",
        "runtimeService"
    ];

    function adminController(
        $rootScope,
        $scope,
        $window,
        $log,
        $location,
        loginService,
        peopleService,
        personService,
        flashService,
        runtimeService
    ){
        var ctrl = this;

        ctrl.refresh = function() {
            flashService.clear(); // clear messages

            if (runtimeService.useLiveData()) {
                $scope.people = peopleService.query({ accountId: loginService.getLoggedInAccountId() }, function() {
                    ////$log.info("People fetched!");
                }, function(response) {
                    var message = "An unexpected error occurred";
                    if (response.status === 404) {
                        message = response.data.message;
                        $log.error("404: " + message);
                    } else if (response.status === 500) {
                        // a nodejs specific error object containing a message and stack
                        if (response.data.error) {
                            message = response.data.error.message;
                            $log.error("500: " + message + ", stack: " + response.data.error.stack);
                        } else {
                            message = response.data.message;
                            $log.error("500: " + message);
                        }
                    } else {
                        $log.error(message);
                    }
                    flashService.add("error", message);
                });
            } else {
                $scope.people = [];
            }
        };

        ctrl.refreshKeyDown = function($event) {
            if ($event.keyCode === 13) { // ENTER
                ctrl.refresh();
            }
        };

        ctrl.add = function() {
            var np = $window.prompt("Please enter a name", "");
            if (np !== null && np !== "") {
                var person = new personService();
                person.name = np;
                person.accountId = loginService.getLoggedInAccountId();
                personService.save(person, function(np) {
                    ////$log.info("Person created!");
                    $scope.people.push(np);
                });
            }
        };

        ctrl.edit = function($index) {
            var person = $scope.people[$index];
            var ep = $window.prompt("Please enter a name", person.name);
            if (ep !== null && ep !== "" && ep !== person.name) {
                person.name = ep;
                personService.update(person, function(ep) {
                    ////$log.info("Person edited!");
                    $scope.people[$index] = ep;
                });
            }
        };

        ctrl.delete = function($index) {
            var person = $scope.people[$index];
            if ($window.confirm("Are you sure you want to delete this person?")) {
                personService.delete({ _id: person._id }, function() {
                    ////$log.info(person.name + " deleted!");
                    $scope.people.splice($index, 1);
                }, function(response) {
                    var message = "An unexpected error occurred";
                    if (response.status === 404) {
                        message = response.data.message;
                        $log.error("404: " + message);
                    } else if (response.status === 500) {
                        // a nodejs specific error object containing a message and stack
                        if (response.data.error) {
                            message = response.data.error.message;
                            $log.error("500: " + message + ", stack: " + response.data.error.stack);
                        } else {
                            message = response.data.message;
                            $log.error("500: " + message);
                        }
                    } else {
                        $log.error(message);
                    }
                    flashService.add("error", message);
                });
            }
        };

        ctrl.unallocate = function($index) {
            var person = $scope.people[$index];
            if ($window.confirm("Are you sure you want to un-allocate this person?")) {
                delete person.allocatedPerson;
                personService.update(person, function(ep) {
                    ////$log.info("Person unallocated!");
                    $scope.people[$index] = ep;
                });
            }
        };

        ctrl.addkeyDown = function($event) {
            if ($event.keyCode === 13) { // ENTER
                ctrl.add();
            }
        };

        ctrl.editKeyDown = function($event, $index) {
            if ($event.keyCode === 13) { // ENTER
                ctrl.edit($index);
            }
        };

        ctrl.deleteKeyDown = function($event, $index) {
            if ($event.keyCode === 13) { // ENTER
                ctrl.delete($index);
            }
        };

        ctrl.unallocateKeyDown = function($event, $index) {
            if ($event.keyCode === 13) { // ENTER
                ctrl.unallocate($index);
            }
        };

        ctrl.shareUrl = function() {
            if (!loginService.getCurrentAccount()) {
                return "";
            }
            return $location.protocol() + "://" + $location.host() + ($location.port() !== 80 ? ":" + $location.port() : "") + "/#/s/" + loginService.getCurrentAccount().shortUrl;
        };

        ctrl.showShareUrl = function() {
            $window.prompt("Copy to clipboard: Ctrl+C, Enter", ctrl.shareUrl());
        };

        ctrl.signOut = function() {
            $rootScope.currentLogin = null;
            $rootScope.currentAccount = null;
            loginService.signOutAndRedirect();
        };

        // load the people
        ctrl.refresh();
    }
})();