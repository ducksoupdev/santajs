(function() {
    "use strict";

    angular
        .module("santaApp")
        .run(runBlock);

    runBlock.$inject = ["$rootScope", "$log", "$location", "loginService"];

    function runBlock($rootScope, $log, $location, loginService) {
        // initialise the login service
        loginService.init();

        var routesThatDoNotRequireAuth = ["/s", "/home", "/signUp", "/login", "/v"];

        // check if current location matches route
        var routeClean = function(route) {
            if (!route || route === "") {
                return true;
            }
            return _.find(routesThatDoNotRequireAuth,
                function (noAuthRoute) {
                    return _.str.startsWith(route, noAuthRoute);
                });
        };

        $rootScope.$on("$routeChangeStart", function (event, next, current) {
            //$log.info("Checking route auth for URL: " + $location.url());

            var rc = routeClean($location.url());

            if (!rc) {
                if (loginService.hasExistingSession() && loginService.isLoggedIn() && loginService.getCurrentLogin()) {
                    // the person is logged in and we have a person
                    // and account object saved in memory so do nothing!
                } else if (loginService.hasExistingSession()) {
                    // verify the cookie
                    loginService.autoLogin()
                        .then(function(login) {
                            // do nothing as the loginService has the session info
                            //$log.info("Autologin was successful");
                            $rootScope.currentLogin = loginService.getCurrentLogin();
                            $rootScope.currentAccount = loginService.getCurrentAccount();
                        }, function(errorMessage) {
                            //$log.info("An auto login error occurred: " + errorMessage.message);
                            // in the event of an error - sign the user out
                            loginService.signOut();
                        });
                } else {
                    //$log.info("Redirecting to login");

                    // redirect back to login
                    var path = $location.path();
                    $location.path("/login").search({ returnPath: path });
                }
            } else {
                // redirect to people if the person is logged in
                if (loginService.hasExistingSession() && loginService.isLoggedIn() && loginService.getCurrentLogin()) {
                    if ($location.path() === "/login") {
                        $location.search("");
                        $location.path("/admin");
                    }
                } else if (loginService.hasExistingSession()) {
                    // verify the cookie
                    loginService.autoLogin()
                        .then(function(login) {
                            // do nothing as the loginService has the session info
                            //$log.info("Autologin was successful");
                            $rootScope.currentLogin = loginService.getCurrentLogin();
                            $rootScope.currentAccount = loginService.getCurrentAccount();
                        }, function(errorMessage) {
                            //$log.info("An auto login error occurred: " + errorMessage.message);
                            // in the event of an error - sign the user out
                            loginService.signOut();
                        });
                }
            }
        });
    }
})();
