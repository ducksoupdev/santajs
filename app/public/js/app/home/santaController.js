(function() {
    "use strict";

    angular.module("santaApp")
        .controller("santaController", santaController);

    santaController.$inject = [
        "$scope",
        "$routeParams",
        "$log",
        "flashService",
        "accountByShortUrlService",
        "personAllocateService"
    ];

    function santaController(
        $scope,
        $routeParams,
        $log,
        flashService,
        accountByShortUrlService,
        personAllocateService
    ){
        var ctrl = this;

        $scope.focusInput = true;
        $scope.name = null;
        $scope.formSubmitted = false;
        $scope.account = null;

        accountByShortUrlService.get({ shortUrl: $routeParams.shortUrl }, function(account) {
            //$log.info("Account fetched by shortUrl: " + $routeParams.shortUrl + ": " + account);
            $scope.account = account;
        }, function(response) {
            var message = "An unexpected error occurred";
            if (response.status === 404) {
                message = response.data.message;
                $log.error("404: " + message);
            } else if (response.status === 500) {
                // a nodejs specific error object containing a message and stack
                if (response.data.error) {
                    message = response.data.error.message;
                    $log.error("500: " + message + ", stack: " + response.data.error.stack);
                } else {
                    message = response.data.message;
                    $log.error("500: " + message);
                }
            } else {
                $log.error(message);
            }
            flashService.add("error", message);
            $scope.formSubmitted = true;
        });

        ctrl.allocate = function() {
            if ($scope.name) {
                personAllocateService.save({ accountId: $scope.account._id }, { name: $scope.name }, function(personAllocated) {
                    $scope.formSubmitted = true;
                    flashService.clear();
                    flashService.add("success", "You're buying a gift for <strong>" + personAllocated.name + "</strong>!");
                }, function(response) {
                    var message = "An unexpected error occurred";
                    if (response.status === 404) {
                        message = response.data.message;
                        $log.error("404: " + message);
                    } else if (response.status === 500) {
                        // a nodejs specific error object containing a message and stack
                        if (response.data.error) {
                            message = response.data.error.message;
                            $log.error("500: " + message + ", stack: " + response.data.error.stack);
                        } else {
                            message = response.data.message;
                            $log.error("500: " + message);
                        }
                    } else {
                        $log.error(message);
                    }
                    flashService.clear();
                    flashService.add("error", message);
                });
            } else {
                flashService.add("error", "Please enter your name!");
            }
        };
    }
})();