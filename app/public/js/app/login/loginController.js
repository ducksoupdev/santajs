(function() {
    "use strict";

    angular.module("santaApp")
        .controller("loginController", loginController);

    loginController.$inject = [
        "$rootScope",
        "$scope",
        "$location",
        "$log",
        "loginService",
        "flashService",
        "personService"
    ];

    function loginController(
        $rootScope,
        $scope,
        $location,
        $log,
        loginService,
        flashService,
        personService
    ){
        var ctrl = this;

        $scope.focusInput = true;
        $scope.person = new personService();

        ctrl.signIn = function() {
            flashService.clear(); // clear messages
            var returnPath = ($location.search()).returnPath;
            ////$log.info("Signing in with email: " + $scope.person.email + " and password: " + $scope.person.password);
            loginService.login($scope.person.email, $scope.person.password)
                .then(function(login) {
                    // set the current login variables
                    $rootScope.currentLogin = loginService.getCurrentLogin();
                    $rootScope.currentAccount = loginService.getCurrentAccount();

                    // redirect to correct page based on entity
                    $location.search("");
                    $location.path("/admin");
                },
                function(errorMessage) {
                    ////$log.info("A login error occurred: " + errorMessage.message);
                    flashService.add("error", errorMessage.message);
                });
        };

        ctrl.cancel = function() {
            $location.path("/home");
        };
    }
})();