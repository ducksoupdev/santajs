(function() {
    "use strict";

    angular.module("santaApp")
        .factory("loginVerifyService", loginVerifyService);

    loginVerifyService.$inject = [
    	"$resource"
    ];

    function loginVerifyService(
    	$resource
    ) {
        return $resource("/api/auth/:code", {}, {
            verify: { method: "GET" }
        });
    }
})();
		