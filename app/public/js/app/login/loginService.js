(function() {
    "use strict";

    angular.module("santaApp")
        .service("loginService", loginService);

    loginService.$inject = [
    	"$q", 
    	"$log", 
    	"$location", 
    	"$timeout", 
    	"loginAuthService",
    	"accountService",
    	"$localStorage",
        "runtimeService"
    ];

    function loginService(
    	$q, 
    	$log, 
    	$location, 
    	$timeout, 
    	loginAuthService,
    	accountService,
    	$localStorage,
        runtimeService
    ) {
        var self = this;
        
        var currentLogin = null,
            currentAccount = null,
            errorMessage = null,
            loggedInAccountId = null,
            loggedInEmail = null,
            loggedInAutoLogin = null;

        self.getCurrentLogin = function() {
            return currentLogin;
        };

        self.getCurrentAccount = function() {
            return currentAccount;
        };

        self.setCurrentAccount = function(account) {
            currentAccount = account;
            loggedInAccountId = account._id;

            // store the account id value in localStorage
            $localStorage.sjsac = account._id;
        };

        self.getErrorMessage = function() {
            return errorMessage;
        };

        self.getLoggedInAccountId = function() {
            return loggedInAccountId;
        };

        self.getLoggedInEmail = function() {
            return loggedInEmail;
        };

        self.getLoggedInAutoLogin = function() {
            return loggedInAutoLogin;
        };

        self.init = function() {
            // check for session info in local storage
            loggedInEmail = $localStorage.sjsem || null;
            loggedInAutoLogin = $localStorage.sjsal || null;
            loggedInAccountId = $localStorage.sjsac || null;
        };

        self.hasExistingSession = function() {
            return (loggedInEmail && loggedInAutoLogin);
        };

        self.isLoggedIn = function() {
            return (currentLogin && currentAccount);
        };

        self.login = function(email, password) {
            // create a deferred object
            var deferred = $q.defer();

            if (runtimeService.useLiveData()) {
                var login = loginAuthService.login({ email: email, password: password },
                    function(response) {
                        currentLogin = login;

                        //$log.info("Account auto login cookie: " + login.autoLogin);
                        // store the auto_login value in localStorage
                        $localStorage.sjsac = login.account;
                        $localStorage.sjsal = login.autoLogin;
                        $localStorage.sjsem = login.email;

                        loggedInAccountId = login.account;
                        loggedInEmail = login.email;
                        loggedInAutoLogin = login.autoLogin;

                        var account = accountService.get({ _id: login.account }, function() {
                            //$log.info("Account fetched: " + account.name);
                            currentAccount = account;
                            deferred.resolve(login);
                        });
                    },
                    function(response) {
                        // error
                        var em = "";
                        if (response.status === 404) {
                            //$log.error("Login not found: " + response.data);
                            em = { message: "Username or password incorrect" };
                        } else if (response.status === 500) {
                            //$log.error("A login error occurred: " + response.data);
                            em = { message: response.data.message };
                        } else {
                            em = { message: "An unexpected error occurred" };
                        }

                        errorMessage = em;
                        deferred.reject(em);
                    }
                );
            } else {
                var login = {
                    account: "1",
                    autoLogin: "autoLogin",
                    email: "ducksoupdev@gmail.com"
                };

                var account = {
                    name: "",
                    shortUrl: "",
                    status: "Active"
                };

                $localStorage.sjsac = login.account;
                $localStorage.sjsal = login.autoLogin;
                $localStorage.sjsem = login.email;

                loggedInAccountId = login.account;
                loggedInEmail = login.email;
                loggedInAutoLogin = login.autoLogin;

                currentAccount = account;
                deferred.resolve(login);
            }


            // return the promise
            return deferred.promise;
        };

        self.autoLogin = function() {
            // create a deferred object
            var deferred = $q.defer();

            var login = loginAuthService.autoLogin({ email: loggedInEmail, autoLogin: loggedInAutoLogin },
                function(response) {
                    //$log.info("Account login on autoLogin: " + login.email);
                    currentLogin = login;

                    var account = accountService.get({ _id: loggedInAccountId }, function() {
                        //$log.info("Account fetched on autoLogin: " + account.name);
                        currentAccount = account;

                        if (currentAccount.status === "Disabled") {
                            deferred.reject("The account " + currentAccount.name + " is currently disabled");
                        } else {
                            deferred.resolve(login);
                        }
                    });
                },
                function(response) {
                    // error
                    errorMessage = response.data;
                    deferred.reject(response.data);
                }
            );

            // return the promise
            return deferred.promise;
        };

        self.signOutAndRedirect = function() {
            // clear the session info
            delete $localStorage.sjsac;
            delete $localStorage.sjsem;
            delete $localStorage.sjsal;

            currentLogin = null;
            currentAccount = null;
            errorMessage = null;
            loggedInAccountId = null;
            loggedInEmail = null;
            loggedInAutoLogin = null;

            // redirect to public home page
            //$log.info("Redirecting to public home page");

            $timeout(function() {
                $location.path("/home");
            }, 300);
        };

        self.signOut = function() {
            // clear the session info
            delete $localStorage.sjsac;
            delete $localStorage.sjsem;
            delete $localStorage.sjsal;

            currentLogin = null;
            currentAccount = null;
            errorMessage = null;
            loggedInAccountId = null;
            loggedInEmail = null;
            loggedInAutoLogin = null;
        };
    }
})();
