(function() {
    "use strict";

    angular.module("santaApp")
        .factory("loginAuthService", loginAuthService);

    loginAuthService.$inject = [
    	"$resource"
    ];

    function loginAuthService(
    	$resource
    ) {
        return $resource("/api/auth", {}, {
            login: { method: "POST" },
            autoLogin: { method: "PUT" }
        });
    }
})();
		