(function() {
    "use strict";

    angular.module("santaApp")
        .factory("personAllocateService", personAllocateService);

    personAllocateService.$inject = [
    	"$resource"
    ];

    function personAllocateService(
    	$resource
    ) {	
        return $resource("/api/allocate/:accountId");
    }
})();
		