(function() {
    "use strict";

    angular.module("santaApp")
        .factory("personService", personService);

    personService.$inject = [
    	"$resource"
    ];

    function personService(
    	$resource
    ) {
        return $resource("/api/person/:_id", { _id: "@_id" }, {
            update: { method: "PUT" }
        });
    }
})();
		