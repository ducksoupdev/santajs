(function() {
    "use strict";

    angular.module("santaApp")
        .factory("peopleService", peopleService);

    peopleService.$inject = [
    	"$resource"
    ];

    function peopleService(
    	$resource
    ) {
        return $resource("/api/people/:accountId");
    }
})();
		