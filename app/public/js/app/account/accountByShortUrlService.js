(function() {
    "use strict";

    angular.module("santaApp")
        .factory("accountByShortUrlService", accountByShortUrlService);

    accountByShortUrlService.$inject = [
    	"$resource"
    ];

    function accountByShortUrlService(
    	$resource
    ) {
        return $resource("/api/accountByShortUrl/:shortUrl");
    }
})();
