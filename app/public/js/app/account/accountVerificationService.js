(function() {
    "use strict";

    angular.module("santaApp")
        .factory("accountVerificationService", accountVerificationService);

    accountVerificationService.$inject = [
    	"$resource"
    ];

    function accountVerificationService(
    	$resource
    ) {
        return $resource("/api/secret/:_id");
    }
})();
		