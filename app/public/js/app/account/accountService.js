(function() {
    "use strict";

    angular.module("santaApp")
        .factory("accountService", accountService);

    accountService.$inject = [
    	"$resource"
    ];

    function accountService(
    	$resource
    ) {
        return $resource("/api/account/:_id", { _id: "@_id" }, {
            update: { method: "PUT" }
        });
    }
})();
		