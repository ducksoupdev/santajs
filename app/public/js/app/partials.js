angular.module('santaApp.partials', ['js/app/admin/admin.html', 'js/app/home/home.html', 'js/app/home/santa.html', 'js/app/login/login.html', 'js/app/signup/signUp.html', 'js/app/signup/verify.html', 'js/app/signup/verifyEmail.html']);

angular.module("js/app/admin/admin.html", []).run(["$templateCache", function ($templateCache) {
  $templateCache.put("js/app/admin/admin.html",
    "<div id=\"left-side\">\n" +
    "    <h2>\n" +
    "        My Account\n" +
    "        <a class=\"button logout\" data-ng-click=\"ctrl.signOut()\">Log out</a>\n" +
    "    </h2>\n" +
    "\n" +
    "    <div ng-repeat=\"m in getMessages()\" id=\"flashService-messages\">\n" +
    "        <div class=\"{{ m.level }}\">\n" +
    "            <span>{{ m.text }}</span>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "\n" +
    "    <h3>People</h3>\n" +
    "\n" +
    "    <p>\n" +
    "        <button data-ng-click=\"ctrl.add()\" data-ng-keydown=\"ctrl.addKeyDown($event)\" tabindex=\"0\">Add Person</button>\n" +
    "        <button data-ng-click=\"ctrl.refresh()\" data-ng-keydown=\"ctrl.refreshKeyDown($event)\" tabindex=\"0\"><i class=\"fa fa-refresh fa-2\"></i></button>\n" +
    "    </p>\n" +
    "    <table>\n" +
    "        <thead>\n" +
    "        <tr>\n" +
    "            <th style=\"border-top-left-radius: 10px;\">Person</th>\n" +
    "            <th>Allocated</th>\n" +
    "            <th style=\"border-top-right-radius: 10px;\">&nbsp;</th>\n" +
    "        </tr>\n" +
    "        </thead>\n" +
    "        <tbody>\n" +
    "        <tr data-ng-repeat=\"person in people\">\n" +
    "            <td data-ng-bind=\"person.name\"></td>\n" +
    "            <td data-ng-bind=\"person.allocatedPerson.name\"></td>\n" +
    "            <td class=\"right\">\n" +
    "                <a data-ng-click=\"ctrl.unallocate($index)\" data-ng-keydown=\"ctrl.unallocateKeyDown($event, $index)\" tabindex=\"0\" data-ng-hide=\"!person.allocatedPerson\"><i class=\"fa fa-link fa-3\"></i></a>\n" +
    "                <a data-ng-click=\"ctrl.edit($index)\" data-ng-keydown=\"ctrl.editKeyDown($event, $index)\" tabindex=\"0\"><i class=\"fa fa-pencil fa-3\"></i></a>\n" +
    "                <a data-ng-hide=\"currentAccount.adminPerson._id == person._id\" data-ng-click=\"ctrl.delete($index)\" data-ng-keydown=\"ctrl.deleteKeyDown($event, $index)\" tabindex=\"0\"><i class=\"fa fa-times-circle fa-3\"></i></a>\n" +
    "            </td>\n" +
    "        </tr>\n" +
    "        </tbody>\n" +
    "    </table>\n" +
    "</div>\n" +
    "<div id=\"right-side\">\n" +
    "    <i class=\"fa fa-comment fa-5 share-this\"></i>\n" +
    "    <span class=\"share-this-span\">Click the link to share!</span>\n" +
    "    <img class=\"bottom\" src=\"img/santa.png\" alt=\"{{title}}\">\n" +
    "    <div class=\"share\">\n" +
    "        <a data-ng-click=\"ctrl.showShareUrl()\" data-ng-bind=\"ctrl.shareUrl()\"></a>\n" +
    "    </div>\n" +
    "</div>\n" +
    "");
}]);

angular.module("js/app/home/home.html", []).run(["$templateCache", function ($templateCache) {
  $templateCache.put("js/app/home/home.html",
    "<div id=\"left-side\">\n" +
    "    <p>It's simple and free!</p>\n" +
    "    <ul>\n" +
    "        <li>Add unlimited people</li>\n" +
    "        <li>Send a unique URL to each person</li>\n" +
    "        <li>Randomly allocates each person a secret santa!</li>\n" +
    "        <li>Each person only gets one secret santa!</li>\n" +
    "        <li>It's free!</li>\n" +
    "    </ul>\n" +
    "    <div>\n" +
    "        <span data-ng-hide=\"currentLogin && currentAccount\"><a class=\"button\" href=\"#/signUp\">Sign-up now!</a></span>\n" +
    "        <span data-ng-hide=\"currentLogin && currentAccount\"><a class=\"button yellow\" href=\"#/login?returnPath=%2Fpeople\">Log in</a></span>\n" +
    "        <span data-ng-hide=\"!currentLogin && !currentAccount\"><a class=\"button yellow\" href=\"#/admin\">My account</a></span>\n" +
    "    </div>\n" +
    "</div>\n" +
    "<div id=\"right-side\">\n" +
    "    <img class=\"bottom\" src=\"img/santa.png\" alt=\"{{title}}\">\n" +
    "</div>\n" +
    "");
}]);

angular.module("js/app/home/santa.html", []).run(["$templateCache", function ($templateCache) {
  $templateCache.put("js/app/home/santa.html",
    "<div id=\"left-side\">\n" +
    "    <div ng-repeat=\"m in getMessages()\" id=\"flashService-messages\">\n" +
    "        <div class=\"{{ m.level }}\">\n" +
    "            <span data-ng-bind-html=\"m.text\"></span>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "    <div data-ng-hide=\"formSubmitted\">\n" +
    "        <p>Enter your name below.</p>\n" +
    "        <form class=\"form-horizontal well\" data-ng-submit=\"ctrl.allocate()\">\n" +
    "            <fieldset>\n" +
    "                <label for=\"name\">Name</label>\n" +
    "                <input type=\"text\" data-focus-me=\"focusInput\" id=\"name\" data-ng-model=\"name\">\n" +
    "            </fieldset>\n" +
    "            <fieldset>\n" +
    "                <button type=\"submit\" class=\"submit\">Submit!</button>\n" +
    "            </fieldset>\n" +
    "        </form>\n" +
    "    </div>\n" +
    "</div>\n" +
    "<div id=\"right-side\">\n" +
    "    <img class=\"bottom\" src=\"img/santa.png\" alt=\"{{title}}\">\n" +
    "</div>\n" +
    "");
}]);

angular.module("js/app/login/login.html", []).run(["$templateCache", function ($templateCache) {
  $templateCache.put("js/app/login/login.html",
    "<div id=\"left-side\">\n" +
    "    <h2>Login</h2>\n" +
    "    <div ng-repeat=\"m in getMessages()\" id=\"flashService-messages\">\n" +
    "        <div class=\"{{ m.level }}\">\n" +
    "            <span>{{ m.text }}</span>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "    <form class=\"form-horizontal well\" data-ng-submit=\"ctrl.signIn()\">\n" +
    "        <fieldset>\n" +
    "            <label for=\"email\">Email</label>\n" +
    "            <input data-ng-model=\"person.email\" class=\"input-block-level\" type=\"email\" id=\"email\" placeholder=\"Email\" required=\"\">\n" +
    "        </fieldset>\n" +
    "        <fieldset>\n" +
    "            <label for=\"password\">Password</label>\n" +
    "            <input data-ng-model=\"person.password\" class=\"input-block-level\" type=\"password\" id=\"password\" placeholder=\"Password\" required=\"\">\n" +
    "        </fieldset>\n" +
    "        <fieldset>\n" +
    "            <button type=\"submit\" class=\"submit\">Log in</button>\n" +
    "        </fieldset>\n" +
    "    </form>\n" +
    "</div>\n" +
    "<div id=\"right-side\">\n" +
    "    <img class=\"bottom\" src=\"img/santa.png\" alt=\"{{title}}\">\n" +
    "</div>\n" +
    "");
}]);

angular.module("js/app/signup/signUp.html", []).run(["$templateCache", function ($templateCache) {
  $templateCache.put("js/app/signup/signUp.html",
    "<div id=\"left-side\">\n" +
    "    <h2>Sign-up</h2>\n" +
    "    <div ng-repeat=\"m in getMessages()\" id=\"flashService-messages\">\n" +
    "        <div class=\"{{ m.level }}\">\n" +
    "            <span>{{ m.text }}</span>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "    <div data-ng-hide=\"formSubmitted\">\n" +
    "        <p>To sign-up, enter your name, email and choose a password.</p>\n" +
    "        <form class=\"form-horizontal well\" data-ng-submit=\"ctrl.signMeUp()\">\n" +
    "            <fieldset>\n" +
    "                <label for=\"accountName\">Name</label>\n" +
    "                <input data-ng-model=\"account.name\" data-focus-me=\"focusInput\" class=\"input-block-level\" type=\"text\" id=\"accountName\" placeholder=\"Name\" required=\"\">\n" +
    "            </fieldset>\n" +
    "            <fieldset>\n" +
    "                <label for=\"email\">Email</label>\n" +
    "                <input data-ng-model=\"account.email\" class=\"input-block-level\" type=\"email\" id=\"email\" placeholder=\"Email\" required=\"\">\n" +
    "            </fieldset>\n" +
    "            <fieldset>\n" +
    "                <label for=\"password\">Password</label>\n" +
    "                <input data-ng-model=\"account.password\" class=\"input-block-level\" type=\"password\" id=\"password\" placeholder=\"Password\" required=\"\">\n" +
    "            </fieldset>\n" +
    "            <fieldset>\n" +
    "                <button type=\"submit\" class=\"submit\">Sign up!</button>\n" +
    "            </fieldset>\n" +
    "        </form>\n" +
    "    </div>\n" +
    "</div>\n" +
    "<div id=\"right-side\">\n" +
    "    <img class=\"bottom\" src=\"img/santa.png\" alt=\"{{title}}\">\n" +
    "</div>\n" +
    "");
}]);

angular.module("js/app/signup/verify.html", []).run(["$templateCache", function ($templateCache) {
  $templateCache.put("js/app/signup/verify.html",
    "<div id=\"left-side\">\n" +
    "    <h2>Activate your account</h2>\n" +
    "    <div ng-repeat=\"m in getMessages()\" id=\"flashService-messages\">\n" +
    "        <div class=\"{{ m.level }}\">\n" +
    "            <span>{{ m.text }}</span>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "    <div data-ng-if=\"displayLogin\">\n" +
    "        <span><a class=\"button yellow\" href=\"#/login?returnPath=%2Fpeople\">Log in</a></span>\n" +
    "    </div>\n" +
    "</div>\n" +
    "<div id=\"right-side\">\n" +
    "    <img class=\"bottom\" src=\"img/santa.png\" alt=\"{{title}}\">\n" +
    "</div>\n" +
    "");
}]);

angular.module("js/app/signup/verifyEmail.html", []).run(["$templateCache", function ($templateCache) {
  $templateCache.put("js/app/signup/verifyEmail.html",
    "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n" +
    "<html xmlns=\"http://www.w3.org/1999/xhtml\">\n" +
    "<head>\n" +
    "    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />\n" +
    "    <title>{{subject}}</title>\n" +
    "    <style type=\"text/css\">\n" +
    "        .activate-button {\n" +
    "            padding: 11px 19px;\n" +
    "            color: #000000;\n" +
    "            text-decoration: none;\n" +
    "            -webkit-border-radius: 10px;\n" +
    "            -moz-border-radius: 10px;\n" +
    "            border-radius: 10px;\n" +
    "            border: 0 solid #330066;\n" +
    "            -webkit-box-shadow: 0 4px 0 rgba(000,000,000,.7),inset 0 0 1px rgba(255,255,255,.6);\n" +
    "            box-shadow: 0 4px 0 rgba(000,000,000,.7),inset 0 0 1px rgba(255,255,255,.6);\n" +
    "        }\n" +
    "    </style>\n" +
    "</head>\n" +
    "<body style=\"background-color: #330066;\">\n" +
    "<table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" height=\"100%\" width=\"100%\" style=\"background-color: #330066;\">\n" +
    "    <tr>\n" +
    "        <td>\n" +
    "            <p style=\"margin: 0; padding: 0; font-size: 0;\">Activate your account</p>\n" +
    "            <img src=\"{{ siteAddress }}/img/email_logo.jpg\" style=\"max-width:280px;\" alt=\"Secret Santa Generator\" />\n" +
    "            <p style=\"font-family: sans-serif; color: #ffffff; font-size: 1.5em; font-weight: normal;\">Thanks for signing up!</p>\n" +
    "            <p style=\"font-family: sans-serif; color: #ffffff; font-size: 1.2em;\">To get started, please click the button below to activate your account.<br />\n" +
    "            <br />\n" +
    "            <br />\n" +
    "            <a style=\"padding: 11px 19px; color: #000000; text-decoration: none; background-color: #fcd700; border-radius: 10px; border: 0 solid #330066;\" class=\"activate-button\" href=\"{{ siteAddress }}{{ activationLink }}\">Activate</a>\n" +
    "            <br />\n" +
    "            <br />\n" +
    "            <br />\n" +
    "            <br />\n" +
    "            <a href=\"{{ siteAddress }}\" style=\"color: #fcd700;\">{{ siteAddress }}</a></p>\n" +
    "        </td>\n" +
    "    </tr>\n" +
    "</table>\n" +
    "</body>\n" +
    "</html>");
}]);
