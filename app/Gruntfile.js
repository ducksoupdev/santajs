(function() {
  /**
   * Created by levym on 16/11/13.
   */
  "use strict";

  module.exports = function(grunt) {
      // load all grunt tasks matching the `grunt-*` pattern
      require("load-grunt-tasks")(grunt);

      grunt.initConfig({
          pkg: grunt.file.readJSON("package.json"),
          mochacli: {
              options: {
                  require: ["expectations"],
                  reporter: "spec",
                  ui: "bdd",
                  recursive: false,
                  debug: true
              },
              unit: {
                  options: {
                      filesRaw: "test/mocha/unit/*.js"
                  }
              },
              int: {
                  options: {
                      filesRaw: "test/mocha/integration/*.js"
                  }
              }
          },
          watch: {
              "mocha-unit": {
                  files: ["test/mocha/unit/*.js", "models/*.js", "util/*.js"],
                  tasks: ["mochacli:unit"]
              },
              "mocha-int": {
                  files: ["test/mocha/integration/*.js", "routes/api/*.js", "models/*.js"],
                  tasks: ["mochacli:int"]
              },
              "karma-unit": {
                  files: ["test/karma/unit/*.js", "public/js/app/*.js"],
                  tasks: ["karma:unit"]
              }
          },
          env: {
              dev: {
                  NODE_ENV: "development"
              },
              test: {
                  NODE_ENV: "test"
              },
              prod: {
                  NODE_ENV: "production"
              }
          },
          shell: {
              "mocha-coverage": {
                  command: "istanbul cover -v --dir coverage/mocha -x public/** node_modules/mocha/bin/_mocha -- -R spec test/mocha/**/*.js"
              }
          },
          bgShell: {
              "run-test-server": {
                  cmd: "node index.js",
                  bg: true
              },
              "server": {
                  cmd: "node index.js",
                  bg: false
              }
          },
          cssmin: {
              app: {
                  files: {
                      "public/css/dist/santa.min.css": ["public/css/tidy/santa.css"]
                  }
              }
          },
          uglify: {
              app: {
                  files: {
                      "public/js/tmp/santa.min.js": ["public/js/app/**/*.js", "!public/js/app/**/*.spec.js", "!public/js/app/partials.js", "!public/js/app/partials-stub.js"]
                  }
              },
              templates: {
                  files: {
                      "public/js/tmp/santa-partials.min.js": ["public/js/app/partials.js"]
                  }
              }
          },
          "sails-linker": {
              "dev-css": {
                  options: {
                      startTag: "<!--STYLES-->",
                      endTag: "<!--STYLES-END-->",
                      fileTmpl: "<link rel=\"stylesheet\" href=\"%s\">",
                      appRoot: "public/"
                  },
                  files: {
                      "public/index.html": ["public/css/styles/*.css"]
                  }
              },
              "dev-js": {
                  options: {
                      startTag: "<!--SCRIPTS-->",
                      endTag: "<!--SCRIPTS-END-->",
                      fileTmpl: "<script src=\"%s\"></script>",
                      appRoot: "public/"
                  },
                  files: {
                      "public/index.html": ["public/bower_components/angular/angular.min.js", "public/bower_components/underscore/underscore-min.js", "public/bower_components/**/*min.js", "!public/bower_components/html5shiv/**/*min.js", "!public/bower_components/respond/**/*min.js", "public/js/app/**/*.js", "!public/js/app/**/*.spec.js", "!public/js/app/partials.js"]
                  }
              },
              "prod-css": {
                  options: {
                      startTag: "<!--STYLES-->",
                      endTag: "<!--STYLES-END-->",
                      fileTmpl: "<link rel=\"stylesheet\" href=\"%s\">",
                      appRoot: "public/"
                  },
                  files: {
                      "public/index.html": "public/css/dist/*.min.css"
                  }
              },
              "prod-js": {
                  options: {
                      startTag: "<!--SCRIPTS-->",
                      endTag: "<!--SCRIPTS-END-->",
                      fileTmpl: "<script src=\"%s\"></script>",
                      appRoot: "public/"
                  },
                  files: {
                      "public/index.html": ["public/js/dist/*.min.js"]
                  }
              }
          },
          karma: {
              unit: {
                  configFile: "karma.conf.js",
                  singleRun: true
              },
              e2e: {
                  configFile: "karma-e2e.conf.js",
                  singleRun: true
              }
          },
          clean: {
              build: ["data", "logs", "public/js/dist"],
              js: ["public/js/tmp"]
          },
          concat: {
              options: {
                  process: false
              },
              dist: {
                  src: ["public/bower_components/angular/angular.min.js", "public/bower_components/underscore/underscore-min.js", "public/bower_components/**/*min.js", "!public/bower_components/html5shiv/**/*min.js", "!public/bower_components/respond/**/*min.js", "public/js/tmp/*.js"],
                  dest: "public/js/dist/combined.min.js"
              }
          },
          compress: {
              main: {
                  options: {
                      mode: "gzip"
                  },
                  files: [
                      // Each of the files in the src/ folder will be output to
                      // the dist/ folder each with the extension .gz.js
                      { expand: true, src: ["public/js/dist/*.min.js"], dest: "", ext: ".min.gz" },
                      { expand: true, src: ["public/css/dist/*.min.css"], dest: "", ext: ".min.gz" }
                  ]
              }
          },
          html2js: {
              options: {
                  base: "public",
                  module: "santaApp.partials"
              },
              main: {
                  src: ["public/js/app/**/*.html"],
                  dest: "public/js/app/partials.js"
              }
          },
          copy: {
              maps: {
                  files: [
                      { expand: true, flatten: true, src: ["public/bower_components/**/*.map"], dest: "public/js/dist/", filter: "isFile" },
                  ]
              }
          },
          uncss: {
              dist: {
                  options: {
                      ignore: ["body", "p", ".chromeframe", "#container", "header", "#header", ".fontface", "h1", "a", "div", "#main", ".ng-view", "footer", "strong", ".info", ".success", ".warning", ".error", ".fa-bitbucket"], // ignore styles in the view/index.html file
                      htmlroot: "public",
                      stylesheets: ["../../../css/styles/reset.css", "../../../css/styles/font-awesome.css", "../../../css/styles/style.css"]
                  },
                  files: {
                      "public/css/tidy/santa.css": ["public/js/app/**/*.html"]
                  }
              }
          }
      });

      grunt.registerTask("mocha-unit", ["env:test", "mochacli:unit"]);
      grunt.registerTask("mocha-int", ["env:test", "mochacli:int"]);
      grunt.registerTask("mocha-full", ["env:test", "mochacli:unit", "mochacli:int", "shell:mocha-coverage"]);

      grunt.registerTask("karma-unit", ["env:test", "karma:unit"]);
      grunt.registerTask("karma-server", ["env:test", "bgShell:run-test-server"]);
      //grunt.registerTask("karma-e2e", ["env:test", "bgShell:run-test-server", "karma:e2e"]);
      grunt.registerTask("karma-e2e", ["karma:e2e"]);
      grunt.registerTask("karma-full", ["env:test", "bgShell:run-test-server", "karma:unit", "karma:e2e"]);

      grunt.registerTask("mocha-watch-unit", ["watch:mocha-unit"]);
      grunt.registerTask("mocha-watch-int", ["watch:mocha-int"]);

      grunt.registerTask("karma-watch-unit", ["watch:karma-unit"]);

      grunt.registerTask("mocha-coverage", ["env:test", "shell:mocha-coverage"]);

      grunt.registerTask("css-min", ["cssmin:app"]);
      grunt.registerTask("uglify-app", ["uglify:app"]);

      grunt.registerTask("build-dev", ["html2js", "sails-linker:dev-css", "sails-linker:dev-js"]);
      grunt.registerTask("build-prod", ["clean:build", "css-tidy", "html2js", "uglify", "concat", "copy", "compress", "sails-linker:prod-css", "sails-linker:prod-js", "clean:js"]);

      grunt.registerTask("server", "bgShell:server");

      grunt.registerTask("css-tidy", ["uncss", "css-min"]);
  };
})();
