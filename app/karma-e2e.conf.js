module.exports = function(config){
    config.set({

    basePath : "",

    reporters: ["progress", "html"],

    files : [
        "node_modules/karma-ng-scenario/lib/angular-scenario.js",
        "node_modules/karma-ng-scenario/lib/adapter.js",
        "test/karma/e2e/*.js"
    ],

    autoWatch : false,

    browsers : ["PhantomJS"],

    frameworks: ["ng-scenario"],

    singleRun : true,

    proxies : {
      "/": "http://localhost:3001/"
    },

    urlRoot: "/__karma__/",

    plugins : [
            "karma-html-reporter",
            "karma-chrome-launcher",
            "karma-firefox-launcher",
            "karma-phantomjs-launcher",
            "karma-jasmine",
            "karma-ng-scenario"
            ],

    htmlReporter: {
        outputDir: "test/karma/reports/e2e/",
        templatePath: "node_modules/karma-html-reporter/jasmine_template.html"
    }

})}

