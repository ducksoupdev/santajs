(function () {
    "use strict";

    module.exports = function(config) {
        config.set({
            basePath: "",
            frameworks: ["jasmine"],
            files: [
                "public/bower_components/angular/angular.js",
                "public/bower_components/angular-resource/angular-resource.js",
                "public/bower_components/angular-route/angular-route.js",
                "public/bower_components/angular-sanitize/angular-sanitize.js",
                "public/bower_components/ngstorage/ngStorage.js",
                "public/bower_components/underscore/underscore.js",
                "public/bower_components/underscore.string/dist/underscore.string.min.js",
                "public/bower_components/angular-mocks/angular-mocks.js",
                "public/js/app/**/*.js"
            ],
            reporters: ["progress"],
            port: 9876,
            colors: true,
            logLevel: config.LOG_INFO,
            autoWatch: false,
            browsers: ["PhantomJS"],
            singleRun: true
        });
    };
})();
