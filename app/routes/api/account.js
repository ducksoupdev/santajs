(function() {
  /**
   * Created by levym on 18/11/13.
   */
  "use strict";

  var restify = require("restify"),
      crypto = require("crypto"),
      fs = require("fs"),
      path = require("path"),
      mustache = require("mustache"),
      nodeMailer = require("nodemailer"),
      account = require("../../models/account"),
      person = require("../../models/person"),
      base64 = require("../../util/base64"),
      Config = require("../../config");

  // create a new config instance
  var config = new Config();

  var handler = function() {
      var _get = function(req, res, next) {
          if (req.params.id) {
              account.get(req.params.id, function(doc) {
                  if (!doc) {
                      res.send(404, { code: "ResourceNotFoundError", message: account.name + " not found!" });
                      return next(false);
                  } else {
                      // remove the secret word
                      var data = doc.toObject();
                      if (data.secretWord) {
                          delete data.secretWord;
                          data.hasSecretWord = true;
                      } else {
                          data.hasSecretWord = false;
                      }
                      res.send(data);
                      return next();
                  }
              }, function(err) {
                  console.log(err);
                  return next(err);
              });
          } else if (req.params.shortUrl) {
              account.find({ shortUrl: req.params.shortUrl }, {}, function(docs) {
                  if (docs.length === 1) {
                      var data = docs[0].toObject();
                      if (data.secretWord) {
                          delete data.secretWord;
                          data.hasSecretWord = true;
                      } else {
                          data.hasSecretWord = false;
                      }
                      res.send(data);
                      return next();
                  } else {
                      res.send(404, { code: "ResourceNotFoundError", message: account.name + " not found!" });
                      return next(false);
                  }
              }, function(err) {
                  return next(err);
              });
          } else {
              return next(new Error("You have requested an invalid URL"));
          }
      };

      var _list = function(req, res, next) {
          account.find({}, {}, function(docs) {
              res.send(docs);
              return next();
          }, function(err) {
              return next(err);
          });
      };

      var _create = function(req, res, next) {
          // perform basic validation
          if (req.params.email && req.params.password && req.params.name) {
              // check for existing person name first
              person.find({ email: req.params.email }, {}, function(docs) {
                  if (docs.length === 1) {
                      return next(new Error("The email address entered is already registered"));
                  } else {
                      // check for a previous shortUrl
                      var ts = new Date().getTime();
                      var shortUrl = base64.fromNumber(ts);

                      account.find({ shortUrl: shortUrl }, {}, function(docs) {
                          if (docs.length === 1) {
                              return next(new Error("The account is already registered, please enter another name"));
                          } else {
                              // create the account first then the person
                              var newSetting = {
                                  "shortUrl": shortUrl
                              };
                              newSetting = _populateValues(newSetting, req.params);
                              account.save(newSetting, function(ns) {
                                  var entityId = ns._id;

                                  // encrypt the password
                                  var shasum = crypto.createHash("sha1");
                                  shasum.update(req.params.password);
                                  var pwd = shasum.digest("hex");

                                  // create a verification code
                                  shasum = crypto.createHash("sha1");
                                  shasum.update(req.params.email + new Date().getTime());
                                  var verificationCode = shasum.digest("hex");

                                  var newPerson = {
                                      "name": req.params.name,
                                      "email": req.params.email,
                                      "password": pwd,
                                      "verificationCode": verificationCode,
                                      "account": entityId
                                  };

                                  person.save(newPerson, function(lg) {
                                      account.save({ _id: ns._id, adminPerson: lg._id }, function(cs) {
                                          if (process.env.NODE_ENV !== "test") {
                                            // send a verification email
                                            var data = {
                                                subject: "Secret Santa Generator - Thanks for signing up!",
                                                currentYear: new Date().getFullYear(),
                                                activationLink: "/#/v/" + verificationCode,
                                                siteAddress: config.url.address
                                            };

                                            // path.resolve removes trailing slash so make sure it's there!
                                            var htmlTemplatePath = path.resolve(__dirname, ".." + path.sep + ".." + path.sep + "public" + path.sep + "js" + path.sep + "app" + path.sep + "signup" + path.sep) + path.sep + "verifyEmail.html";

                                            fs.readFile(htmlTemplatePath, "utf8", function (err, template) {
                                                if (err) {
                                                    return next(err);
                                                }

                                                var transport = nodeMailer.createTransport(config.smtp);

                                                var textMessage = "Thanks for signing up!\r\n\r\n" +
                                                    "To get started, please click the link below to activate your account.\r\n\r\n" +
                                                    "{{ siteAddress }}{{ activationLink }}\r\n\r\n\r\n" +
                                                    "--------------------------------------------------\r\n\r\n" +
                                                    "{{ siteAddress }}";

                                                var html = mustache.render(template, data);
                                                var text = mustache.render(textMessage, data);

                                                // send the email
                                                var message = {
                                                    // sender info
                                                    from: "Secret Santa Generator <santajs@ducksoupdev.co.uk>",

                                                    // Comma separated list of recipients
                                                    to: lg.name + " <" + lg.email + ">",

                                                    // Subject of the message
                                                    subject: data.subject,

                                                    // plaintext body
                                                    text: text,

                                                    // HTML body
                                                    html: html
                                                };

                                                transport.sendMail(message, function(error) {
                                                    if (error){
                                                        return next(new Error("Unable to send verification email: " + error.message));
                                                    } else {
                                                        // if you don't want to use this transport object anymore, uncomment following line
                                                        transport.close(); // close the connection pool

                                                        // return the new account as json
                                                        res.send(201, cs);
                                                        return next();
                                                    }
                                                });
                                            });
                                          } else {
                                            // return the new account as json
                                            res.send(201, cs);
                                            return next();
                                          }
                                      }, function(err) {
                                          return next(err);
                                      });
                                  }, function(err) {
                                      return next(err);
                                  });
                              }, function(err) {
                                  return next(err);
                              });
                          }
                      }, function(err) {
                          return next(err);
                      });
                  }
              }, function(err) {
                  return next(err);
              });
          } else {
              return next(new Error("Please enter a name, email and password for the account"));
          }
      };

      var _update = function(req, res, next) {
          if (req.params.id) {
              account.get(req.params.id, function(es) {
                  es = _populateValues(es, req.params);
                  account.save(es, function(us) {
                      res.send(us);
                      return next();
                  }, function(err) {
                      return next(err);
                  });
              }, function(err) {
                  return next(err);
              });
          } else {
              return next(new Error("You have requested an invalid URL"));
          }
      };

      var _verify = function(req, res, next) {
          if (req.params.id && req.params.secretWord) {
              account.find({ _id: req.params.id, secretWord: req.params.secretWord }, {}, function(docs) {
                  if (docs.length === 1) {
                      res.send({ message: "The secret word is correct!" });
                      return next();
                  } else {
                      res.send(404, { code: "ResourceNotFoundError", message: "The account secret word is incorrect!" });
                      return next(false);
                  }
              }, function(err) {
                  return next(err);
              });
          } else {
              return next(new Error("You have requested an invalid URL"));
          }
      };

      var _populateValues = function(obj, reqBody) {
          if (typeof(reqBody.name) !== "undefined" && reqBody.name !== "") { obj.name = reqBody.name; }
          if (typeof(reqBody.secretWord) !== "undefined" && reqBody.secretWord !== "") { obj.secretWord = reqBody.secretWord; }
          if (typeof(reqBody.status) !== "undefined" && reqBody.status !== "") { obj.status = reqBody.status; }
          return obj;
      };

      return {
          get: _get,
          create: _create,
          update: _update,
          list: _list,
          verification: _verify
      };

  }();

  module.exports = handler;
})();
