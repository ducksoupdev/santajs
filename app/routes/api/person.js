(function() {
  /**
   * Created by levym on 18/11/13.
   */
  "use strict";

  var restify = require("restify"),
      crypto = require("crypto"),
      person = require("../../models/person"),
      __ = require("underscore")._,
      async = require("async"),
      allocate = require("../../util/allocate");

  var secretKey = "OLOoEaxshcbEWgcYEodDuckcSt0PjcW8hXrSantaWhoopkwMarxUtbR5sOBoaM54";

  var handler = function() {
      var _get = function(req, res, next) {
          if (req.params.id) {
              person.get(req.params.id, function(doc) {
                  if (!doc) {
                      res.send(404, { code: "ResourceNotFoundError", message: person.name + " not found!" });
                      return next(false);
                  } else {
                      var data = doc.toObject();
                      if (data.password) {
                          delete data.password;
                          data.hasPassword = true;
                      } else {
                          data.hasPassword = false;
                      }
                      res.send(data);
                      return next();
                  }
              }, function(err) {
                  return next(err);
              });
          } else {
              return next(new Error("You have requested an invalid URL"));
          }
      };

      var _list = function(req, res, next) {
          if (req.params.accountId) {
              person.find({
                  account: req.params.accountId
              }, {}, function(docs) {
                  var newDocs = docs.slice(0);
                  __.each(newDocs, function(e, i) {
                      if (e.password) {
                          delete e.password;
                          e.hasPassword = true;
                      } else {
                          e.hasPassword = false;
                      }
                  });

                  res.send(newDocs);
                  return next();
              }, function(err) {
                  return next(err);
              });
          } else {
              return next(new Error("You have requested an invalid URL"));
          }
      };

      var _create = function(req, res, next) {
          if (req.params instanceof Array) {
              var docs = req.params;
              var peopleSaved = [];
              var errors = [];
              async.eachSeries(docs, function(d, cb) {
                  if (d.name && d.accountId) {
                      // check for existing person name first
                      person.find({ name: d.name, account: d.accountId }, {}, function(docs) {
                          if (docs.length === 1) {
                              return next(new Error("The name entered is already added!"));
                          } else {
                              var newPerson = _populateValues({}, d);

                              // if we have an email - double-check it's not already registered
                              if (d.email) {
                                  person.find({ email: d.email }, {}, function(docs) {
                                      if (docs.length === 1) {
                                          return next(new Error("The email address entered is already registered!"));
                                      } else {
                                          // add the new person
                                          newPerson.email = d.email;
                                          var shasum;

                                          // encrypt the password
                                          if (d.password) {
                                              shasum = crypto.createHash("sha1");
                                              shasum.update(d.password);
                                              var pwd = shasum.digest("hex");
                                              newPerson.password = pwd;
                                          }

                                          // create a verification code
                                          shasum = crypto.createHash("sha1");
                                          shasum.update(d.email + new Date().getTime());
                                          var verificationCode = shasum.digest("hex");
                                          newPerson.verificationCode = verificationCode;

                                          person.save(newPerson, function(lg) {
                                              var data = lg.toObject();
                                              if (data.password) {
                                                  delete data.password;
                                                  data.hasPassword = true;
                                              } else {
                                                  data.hasPassword = false;
                                              }

                                              peopleSaved.push(data);
                                              cb();

                                          }, function(err) {
                                              errors.push(err.message);
                                              cb();
                                          });
                                      }
                                  }, function(err) {
                                      errors.push(err.message);
                                      cb();
                                  });
                              } else {
                                  person.save(newPerson, function(lg) {
                                      peopleSaved.push(lg);
                                      cb();
                                  }, function(err) {
                                      errors.push(err.message);
                                      cb();
                                  });
                              }
                          }
                      });
                  }
              }, function() {
                  res.send(201, {
                      "peopleSaved": peopleSaved,
                      "errors": errors
                  });
                  return next();
              });
          } else {
              // perform basic validation
              if (req.params.name && req.params.accountId) {
                  // check for existing person name first
                  person.find({ name: req.params.name, account: req.params.accountId }, {}, function(docs) {
                      if (docs.length === 1) {
                          return next(new Error("The name entered is already added!"));
                      } else {
                          var newPerson = _populateValues({}, req.params);

                          // if we have an email - double-check it's not already registered
                          if (req.params.email) {
                              person.find({ email: req.params.email }, {}, function(docs) {
                                  if (docs.length === 1) {
                                      return next(new Error("The email address entered is already registered!"));
                                  } else {
                                      // add the new person
                                      newPerson.email = req.params.email;
                                      var shasum;

                                      // encrypt the password
                                      if (req.params.password) {
                                          shasum = crypto.createHash("sha1");
                                          shasum.update(req.params.password);
                                          var pwd = shasum.digest("hex");
                                          newPerson.password = pwd;
                                      }

                                      // create a verification code
                                      shasum = crypto.createHash("sha1");
                                      shasum.update(req.params.email + new Date().getTime());
                                      var verificationCode = shasum.digest("hex");
                                      newPerson.verificationCode = verificationCode;

                                      person.save(newPerson, function(lg) {
                                          var data = lg.toObject();
                                          if (data.password) {
                                              delete data.password;
                                              data.hasPassword = true;
                                          } else {
                                              data.hasPassword = false;
                                          }

                                          res.send(201, data);
                                          return next();

                                      }, function(err) {
                                          return next(err);
                                      });
                                  }
                              }, function(err) {
                                  return next(err);
                              });
                          } else {
                              person.save(newPerson, function(lg) {
                                  // return the new person as json
                                  res.send(201, lg);
                                  return next();
                              }, function(err) {
                                  return next(err);
                              });
                          }
                      }
                  }, function(err) {
                      return next(err);
                  });
              } else {
                  return next(new Error("A name and accountId are required for the person"));
              }
          }
      };

      var _update = function(req, res, next) {
          if (req.params.id) {
              person.get(req.params.id, function(el1) {
                  // make a copy of the object and populate it
                  var el = el1.toObject();
                  el = _populateValues(el, req.params);

                  // do we have a password to update
                  if (req.params.password) {
                      var shasum = crypto.createHash("sha1");
                      shasum.update(req.params.password);
                      var pwd = shasum.digest("hex");
                      el.password = pwd;
                  } else {
                      delete el.password;
                  }

                  // do we have an allocated person to save or clear?
                  if (el.allocatedPerson && typeof req.params.allocatedPerson === "undefined") {
                      el.allocatedPerson = null;
                  }

                  person.save(el, function(ul) {
                      res.send(ul);
                      return next();
                  }, function(err) {
                      return next(err);
                  });
              }, function(err) {
                  return next(err);
              });
          } else {
              return next(new Error("You have requested an invalid URL"));
          }
      };

      var _delete = function(req, res, next) {
          if (req.params.id) {
              person.exists({ _id: req.params.id }, function(exists) {
                  if (exists) {
                      person.delete(req.params.id, function() {
                          res.send(204, "");
                          return next();
                      }, function(err) {
                          return next(err);
                      });
                  } else {
                      res.send(404, { code: "ResourceNotFoundError", message: "Person not found!" });
                      return next(false);
                  }
              }, function(err) {
                  return next(err);
              });
          } else {
              return next(new Error("You have requested an invalid URL"));
          }
      };

      var _verify = function(req, res, next) {
          if (req.params.verificationCode) {
              person.find({ verificationCode: req.params.verificationCode }, {}, function(docs) {
                  if (docs.length === 1) {
                      var lg = docs[0];
                      if (lg.verifiedDate) {
                          res.send(202, { message: "Your account was verified on: " + lg.verifiedDate });
                          return next();
                      } else {
                          // update verified date
                          lg.verifiedDate = new Date();
                          person.save(lg, function(sl) {
                              lg.password = null;
                              res.send(204, lg);
                              return next();
                          }, function(err) {
                              return next(err);
                          });
                      }
                  } else {
                      res.send(404, { code: "ResourceNotFoundError", message: "The person cannot be found by the supplied verification code" });
                      return next(false);
                  }
              }, function(err) {
                  return next(err);
              });
          } else {
              return next(new Error("You have requested an invalid URL"));
          }
      };

      var _auth = function(req, res, next) {
          if (req.params.email && req.params.password) {
              // encrypt the password
              var shasum = crypto.createHash("sha1");
              shasum.update(req.params.password);
              var pwd = shasum.digest("hex");

              person.find({ email: req.params.email, password: pwd }, {}, function(docs) {
                  if (docs.length === 1) {
                      var lg = docs[0];
                      if (!lg.loginCount) {
                          lg.loginCount = 1;
                      } else {
                          lg.loginCount = lg.loginCount + 1;
                      }
                      lg.lastPersonDate = new Date();
                      person.save(lg, function(sl) {
                          // create an auto person hash that can be shared with other parts of the site through a cookie
                          shasum = crypto.createHash("sha1");
                          shasum.update(sl.email + secretKey);
                          sl.autoLogin = shasum.digest("hex");
                          sl.password = null;
                          res.send(sl);
                          return next();

                      }, function(err) {
                          return next(err);
                      });
                  } else {
                      res.send(404, { code: "ResourceNotFoundError", message: "The requested person cannot be found" });
                      return next(false);
                  }
              }, function(err) {
                  return next(err);
              });
          } else {
              return next(new Error("Email and password are required to sign-in"));
          }
      };

      var _autoLogin = function(req, res, next) {
          if (req.params.email && req.params.autoLogin) {
              var shasum = crypto.createHash("sha1");
              shasum.update(req.params.email + secretKey);
              var val = shasum.digest("hex");
              if (val === req.params.autoLogin) {
                  // auto login is correct
                  person.find({ email: req.params.email }, {}, function(docs) {
                      if (docs.length === 1) {
                          var lg = docs[0];
                          res.send(lg);
                          return next();
                      } else {
                          res.send(404, { code: "ResourceNotFoundError", message: "The requested person cannot be found" });
                          return next(false);
                      }
                  }, function(err) {
                      return next(err);
                  });
              } else {
                  return next(new Error("Auto login is incorrect"));
              }
          } else {
              return next(new Error("No email or auto login supplied"));
          }
      };

      var _allocate = function(req, res, next) {
          if (req.params.accountId && req.params.name) {
              person.find({
                  account: req.params.accountId
              }, {}, function(docs) {
                  var currentPerson = null;

                  // get the current person
                  __.each(docs, function(e, i) {
                      if (e.name.toLowerCase() === req.params.name.toLowerCase()) {
                          currentPerson = e;
                      }
                  });

                  if (currentPerson) {
                      if (currentPerson.allocatedPerson) {
                          res.send(currentPerson.allocatedPerson);
                          return next();
                      } else {
                          var thisPerson = allocate.fromList(docs, req.params.name);
                          if (thisPerson) {
                              // save the allocated person
                              currentPerson.allocatedPerson = thisPerson._id;
                              person.save(currentPerson, function(up) {
                                  res.send(thisPerson);
                                  return next();
                              }, function(err) {
                                  return next(err);
                              });
                          } else {
                              res.send(204, "");
                              return next();
                          }
                      }
                  } else {
                      return next(new Error(req.params.name + " does not exist!"));
                  }
              }, function(err) {
                  return next(err);
              });
          } else {
              return next(new Error("You have requested an invalid URL"));
          }
      };

      var _populateValues = function(obj, reqBody) {
          if (typeof(reqBody.name) !== "undefined" && reqBody.name !== "") { obj.name = reqBody.name; }
          if (typeof(reqBody.email) !== "undefined" && reqBody.email !== "") { obj.email = reqBody.email; }
          if (typeof(reqBody.accountId) !== "undefined" && reqBody.accountId !== "") { obj.account = reqBody.accountId; }
          if (typeof(reqBody.allocatedPersonId) !== "undefined" && reqBody.allocatedPersonId !== "") { obj.allocatedPerson = reqBody.allocatedPersonId; }
          return obj;
      };

      return {
          get: _get,
          create: _create,
          update: _update,
          delete: _delete,
          list: _list,
          verification: _verify,
          authenticate: _auth,
          autoLogin: _autoLogin,
          allocate: _allocate
      };

  }();

  module.exports = handler;
})();
