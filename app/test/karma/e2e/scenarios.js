"use strict";

/* http://docs.angularjs.org/guide/dev_guide.e2e-testing */

describe("Secret Santa Generator", function () {

    beforeEach(function () {
        browser().navigateTo("/");
    });

    it("should automatically redirect to /home when location hash/fragment is empty", function () {
        pause();
        expect(browser().location().url()).toBe("/home");
    });

    describe("home", function () {
        beforeEach(function () {
            browser().navigateTo("#/home");
        });

        it("should render home when user navigates to /home", function () {
            pause();
            expect(element("[ng-view] p:first").text()).toMatch(/It\\'s simple and free\\!/);
        });
    });


    describe("Sign up", function () {
        beforeEach(function () {
            browser().navigateTo("#/signUp");
        });

        it("should render sign-up when user navigates to /signUp", function () {
            pause();
            expect(element("[ng-view] h2").text()).toMatch(/Sign-up/);
            pause();
        });
    });
});
