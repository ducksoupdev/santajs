(function () {
    /**
     * Created by levym on 16/11/13.
     */
    "use strict";

    var request = require("supertest"),
        restify = require("restify"),
        mongoose = require("mongoose"),
        account = require("../../../routes/api/account");

    var config = {
        "url": {
            "address": "http://localhost:2370"
        },
        "db": {
            "mongodb": "mongodb://localhost/santajs_test"
        },
        "logger": {
            "exception": "../../../logs/exceptions_account_int.log"
        }
    };

    describe("Account API", function () {
        var server = null;

        before(function (done) {
            // create restify server
            server = restify.createServer();

            // body parser for post requests
            server.use(restify.bodyParser({
                mapParams: true
            }));

            server.get("/api/accountByShortUrl/:shortUrl", account.get);
            server.get("/api/account/:id", account.get);
            server.post("/api/account", account.create);
            server.put("/api/account/:id", account.update);
            server.post("/api/secret/:id", account.verification);

            server.listen(2370, function () {
                // connect to mongoose
                mongoose.connect(config.db.mongodb);
                mongoose.connection.on("open", function () {
                    // console.log("Connection to: " + config.db.mongodb + " opened!");
                });

                done();
            });
        });

        after(function (done) {
            // close the restify server
            server.close();

            // drop test database
            mongoose.connection.db.dropDatabase(function (err) {
                if (err) {
                    console.log(err);
                }
                mongoose.connection.close(done);
            });
        });

        describe("Create a new account with person login", function () {
            it("Should create a new account with person login", function (done) {
                var newAccount = {
                    name: "Test 5",
                    email: "test@test.com",
                    password: "test"
                };

                request(config.url.address)
                    .post("/api/account")
                    .send(newAccount)
                    .end(function (err, res) {
                        if (err) {
                            console.log(err);
                            throw err;
                        }
                        expect(res.status).toBe(201);
                        expect(res.body.name).toEqual("Test 5");
                        done();
                    });
            });
        });

        describe("Check for duplicate login", function () {
            var newAccount = {
                name: "Test 5",
                email: "test@test.com",
                password: "test"
            };

            beforeEach(function (done) {
                request(config.url.address)
                    .post("/api/account")
                    .send(newAccount)
                    .end(function (err, res) {
                        if (err) {
                            console.log(err.message);
                            throw err;
                        }
                        done();
                    });
            });

            it("Should throw an error if a duplicate email is found", function (done) {
                request(config.url.address)
                    .post("/api/account")
                    .send(newAccount)
                    .end(function (err, res) {
                        if (err) {
                            console.log(err.message);
                            throw err;
                        }
                        expect(res.status).toBe(500);
                        expect(res.body.message).toEqual("The email address entered is already registered");
                        done();
                    });
            });
        });

        describe("Get an existing account", function () {
            var newAccount = {
                name: "Test 6",
                email: "test6@test.com",
                password: "test",
                secretWord: "test6"
            };
            var savedAccount = null;

            before(function (done) {
                request(config.url.address)
                    .post("/api/account")
                    .send(newAccount)
                    .end(function (err, res) {
                        if (err) {
                            console.log(err.message);
                            throw err;
                        } else {
                            savedAccount = res.body;
                        }
                        done();
                    });
            });

            it("Should throw an error is the ID is not valid", function (done) {
                request(config.url.address)
                    .get("/api/account/test12345")
                    .end(function (err, res) {
                        if (err) {
                            console.log(err.message);
                            throw err;
                        }
                        expect(res.status).toBe(500);
                        expect(res.body.message).toEqual("Invalid account ID");
                        done();
                    });
            });

            it("Should return a 404 if the account is not found", function (done) {
                request(config.url.address)
                    .get("/api/account/41224d776a326fb40f000001")
                    .end(function (err, res) {
                        if (err) {
                            console.log(err.message);
                            throw err;
                        }
                        expect(res.status).toBe(404);
                        expect(res.body.message).toEqual("Account not found!");
                        done();
                    });
            });

            it("Should fetch an existing account", function (done) {
                request(config.url.address)
                    .get("/api/account/" + savedAccount._id)
                    .end(function (err, res) {
                        if (err) {
                            console.log(err.message);
                            throw err;
                        }
                        expect(res.status).toBe(200);
                        expect(res.body.name).toEqual("Test 6");
                        done();
                    });
            });

            it("Should fetch an existing account by short URL", function (done) {
                request(config.url.address)
                    .get("/api/accountByShortUrl/" + savedAccount.shortUrl)
                    .end(function (err, res) {
                        if (err) {
                            console.log(err.message);
                            throw err;
                        }
                        expect(res.status).toBe(200);
                        expect(res.body.name).toEqual("Test 6");
                        done();
                    });
            });

            it("Should have a null secret word", function (done) {
                request(config.url.address)
                    .get("/api/account/" + savedAccount._id)
                    .end(function (err, res) {
                        if (err) {
                            console.log(err.message);
                            throw err;
                        }
                        expect(res.status).toBe(200);
                        expect(res.body.hasSecretWord).toBeTruthy();
                        done();
                    });
            });
        });

        describe("Update an existing account", function () {
            var newAccount = {
                name: "Test 7",
                email: "test7@test.com",
                password: "test"
            };
            var savedAccount = null;

            beforeEach(function (done) {
                request(config.url.address)
                    .post("/api/account")
                    .send(newAccount)
                    .end(function (err, res) {
                        if (err) {
                            console.log(err.message);
                            throw err;
                        } else {
                            savedAccount = res.body;
                        }
                        done();
                    });
            });

            it("Should update an existing account", function (done) {
                request(config.url.address)
                    .put("/api/account/" + savedAccount._id)
                    .send({
                        _id: savedAccount._id,
                        name: "Test 7 update!"
                    })
                    .end(function (err, res) {
                        if (err) {
                            console.log(err.message);
                            throw err;
                        }
                        expect(res.status).toBe(200);
                        expect(res.body.name).toEqual("Test 7 update!");
                        done();
                    });
            });
        });

        describe("Verify a secret word", function () {
            var newAccount = {
                name: "test8",
                email: "test8@test.com",
                password: "test8",
                secretWord: "test"
            };
            var savedAccount = null;

            before(function (done) {
                request(config.url.address)
                    .post("/api/account")
                    .send(newAccount)
                    .end(function (err, res) {
                        if (err) {
                            console.log(err.message);
                            throw err;
                        } else {
                            savedAccount = res.body;
                        }
                        done();
                    });
            });

            it("Should verify the secret word", function (done) {
                request(config.url.address)
                    .post("/api/secret/" + savedAccount._id)
                    .send({secretWord: "test"})
                    .end(function (err, res) {
                        if (err) {
                            console.log(err.message);
                            throw err;
                        }
                        expect(res.status).toBe(200);
                        done();
                    });
            });

            it("Should return a 404 if no account can be found with the secret word", function (done) {
                request(config.url.address)
                    .post("/api/secret/" + savedAccount._id)
                    .send({secretWord: "gfghfgh"})
                    .end(function (err, res) {
                        if (err) {
                            console.log(err.message);
                            throw err;
                        }
                        expect(res.status).toBe(404);
                        expect(res.body.message).toBe("The account secret word is incorrect!");
                        done();
                    });
            });
        });
    });
})();
