(function() {
  /**
   * Created by levym on 18/11/13.
   */
  "use strict";

  var request = require("supertest"),
      restify = require("restify"),
      mongoose = require("mongoose"),
      person = require("../../../routes/api/person"),
      account = require("../../../routes/api/account");

  var config = {
      "url": {
          "address": "http://localhost:2370"
      },
      "db": {
          "mongodb": "mongodb://localhost/santa_test"
      },
      "logger": {
          "exception": "../../logs/exceptions_person_int.log"
      }
  };

  describe("Person API", function() {
      var server = null;

      before(function(done) {
          // create restify app
          server = restify.createServer();

          // body parser for post requests
          server.use(restify.bodyParser({
            mapParams: true
          }));

          server.get("/api/person/:id", person.get);
          server.post("/api/person", person.create);
          server.put("/api/person/:id", person.update);
          server.del("/api/person/:id", person.delete);
          server.get("/api/people/:accountId", person.list);
          server.post("/api/auth", person.authenticate);
          server.put("/api/auth", person.autoLogin);
          server.get("/api/auth/:verificationCode", person.verification);
          server.post("/api/allocate/:accountId", person.allocate);
          server.post("/api/account", account.create);

          server.listen(2370, function() {
              // connect to mongoose
              mongoose.connect(config.db.mongodb);
              mongoose.connection.on("open", function () {
                  // console.log("Connection to: " + config.db.mongodb + " opened!");
              });

              done();
          });

      });

      after(function(done) {
          // close express server
          server.close();

          // drop test database
          mongoose.connection.db.dropDatabase(function(err) {
              if (err) {
                  console.log(err);
              }
              mongoose.connection.close(done);
          });
      });

      describe("Create a new person", function() {
          it("Should create a new person", function(done) {
              var newPerson = {
                  name: "test",
                  email: "test@test.com",
                  password: "test",
                  accountId: "test"
              };

              request(config.url.address)
                  .post("/api/person")
                  .send(newPerson)
                  .end(function(err, res) {
                      if (err) {
                          console.log(err.message);
                          throw err;
                      }
                      expect(res.status).toBe(201);
                      expect(res.body.email).toEqual("test@test.com");
                      done();
                  });
          });
      });

      describe("Check for duplicate person", function() {
          var newAccount = {
              name: "Test 2",
              email: "test2@test.com",
              password: "test",
              secretWord: "test2"
          };

          var newPerson = {
              name: "Test 2",
              email: "test2@test.com",
              password: "test"
          };

          var savedAccount = null;

          before(function(done) {
              request(config.url.address)
                  .post("/api/account")
                  .send(newAccount)
                  .end(function(err, res) {
                      if (err) {
                          console.log(err.message);
                          throw err;
                      } else {
                          savedAccount = res.body;
                      }
                      done();
                  });
          });

          it("Should throw an error if a duplicate name is found", function(done) {
              newPerson.accountId = savedAccount._id;
              request(config.url.address)
                  .post("/api/person")
                  .send(newPerson)
                  .end(function(err, res) {
                      if (err) {
                          console.log(err.message);
                          throw err;
                      }
                      expect(res.status).toBe(500);
                      expect(res.body.message).toEqual("The name entered is already added!");
                      done();
                  });
          });

          it("Should throw an error if a duplicate email is found", function(done) {
              newPerson.name = "test3a";
              request(config.url.address)
                  .post("/api/person")
                  .send(newPerson)
                  .end(function(err, res) {
                      if (err) {
                          console.log(err.message);
                          throw err;
                      }
                      expect(res.status).toBe(500);
                      expect(res.body.message).toEqual("The email address entered is already registered!");
                      done();
                  });
          });
      });

      describe("Get an existing person", function() {
          var newPerson = {
              name: "test3",
              email: "test3@test.com",
              password: "test3",
              accountId: "test"
          };
          var savedPerson = null;

          beforeEach(function(done) {
              request(config.url.address)
                  .post("/api/person")
                  .send(newPerson)
                  .end(function(err, res) {
                      if (err) {
                          console.log(err.message);
                          throw err;
                      } else {
                          savedPerson = res.body;
                      }
                      done();
                  });
          });

          it("Should fetch an existing person", function(done) {
              request(config.url.address)
                  .get("/api/person/" + savedPerson._id)
                  .end(function(err, res) {
                      if (err) {
                          console.log(err.message);
                          throw err;
                      }
                      expect(res.status).toBe(200);
                      expect(res.body.email).toEqual("test3@test.com");
                      done();
                  });
          });
      });

      describe("Update an existing person", function() {
          var newPerson = {
              name: "test4",
              email: "test4@test.com",
              password: "test4",
              accountId: "test"
          };
          var savedPerson = null;

          beforeEach(function(done) {
              request(config.url.address)
                  .post("/api/person")
                  .send(newPerson)
                  .end(function(err, res) {
                      if (err) {
                          console.log(err.message);
                          throw err;
                      } else {
                          savedPerson = res.body;
                      }
                      done();
                  });
          });

          it("Should update an existing person", function(done) {
              request(config.url.address)
                  .put("/api/person/" + savedPerson._id)
                  .send({
                      _id: savedPerson._id,
                      name: "test5"
                  })
                  .end(function(err, res) {
                      if (err) {
                          console.log(err.message);
                          throw err;
                      }
                      expect(res.status).toBe(200);
                      expect(res.body.name).toEqual("test5");
                      done();
                  });
          });
      });

      describe("Verify a person", function() {
          var newPerson = {
              name: "test6",
              email: "test6@test.com",
              password: "test6",
              accountId: "test"
          };
          var savedPerson = null;

          before(function(done) {
              request(config.url.address)
                  .post("/api/person")
                  .send(newPerson)
                  .end(function(err, res) {
                      if (err) {
                          console.log(err.message);
                          throw err;
                      } else {
                          savedPerson = res.body;
                      }
                      done();
                  });
          });

          it("Should verify the supplied person", function(done) {
              request(config.url.address)
                  .get("/api/auth/" + savedPerson.verificationCode)
                  .end(function(err, res) {
                      if (err) {
                          console.log(err.message);
                          throw err;
                      }
                      expect(res.status).toBe(204);
                      done();
                  });
          });

          it("Should return a 202 when the person has already been verified", function(done) {
              request(config.url.address)
                  .get("/api/auth/" + savedPerson.verificationCode)
                  .end(function(err, res) {
                      if (err) {
                          console.log(err.message);
                          throw err;
                      }
                      expect(res.status).toBe(202);
                      done();
                  });
          });

          it("Should return a 404 if no person can be found with the verification code", function(done) {
              request(config.url.address)
                  .get("/api/auth/gdhsajgfhdsakghjagkshdshf")
                  .end(function(err, res) {
                      if (err) {
                          console.log(err.message);
                          throw err;
                      }
                      expect(res.status).toBe(404);
                      expect(res.body.message).toBe("The person cannot be found by the supplied verification code");
                      done();
                  });
          });
      });

      describe("Authenticate a person", function() {
          var newPerson = {
              name: "test7",
              email: "test7@test.com",
              password: "test7",
              accountId: "test"
          };
          var savedPerson = null;

          before(function(done) {
              request(config.url.address)
                  .post("/api/person")
                  .send(newPerson)
                  .end(function(err, res) {
                      if (err) {
                          console.log(err.message);
                          throw err;
                      } else {
                          savedPerson = res.body;
                      }
                      done();
                  });
          });

          it("Should authenticate an existing person", function(done) {
              request(config.url.address)
                  .post("/api/auth")
                  .send({
                      email: "test7@test.com",
                      password: "test7"
                  })
                  .end(function(err, res) {
                      if (err) {
                          console.log(err.message);
                          throw err;
                      }
                      expect(res.status).toBe(200);
                      expect(res.body.autoLogin).toBeDefined();
                      savedPerson.autoLogin = res.body.autoLogin;
                      done();
                  });
          });

          it("Should throw an error if no email and password are supplied", function(done) {
              request(config.url.address)
                  .post("/api/auth")
                  .send({})
                  .end(function(err, res) {
                      if (err) {
                          console.log(err.message);
                          throw err;
                      }
                      expect(res.status).toBe(500);
                      expect(res.body.message).toBe("Email and password are required to sign-in");
                      done();
                  });
          });

          it("Should return a 404 if no person can be found", function(done) {
              request(config.url.address)
                  .post("/api/auth")
                  .send({
                      email: "pants",
                      password: "pants2"
                  })
                  .end(function(err, res) {
                      if (err) {
                          console.log(err.message);
                          throw err;
                      }
                      expect(res.status).toBe(404);
                      expect(res.body.message).toBe("The requested person cannot be found");
                      done();
                  });
          });

          it("Should auto-login an existing person", function(done) {
              request(config.url.address)
                  .put("/api/auth")
                  .send({
                      email: "test7@test.com",
                      autoLogin: savedPerson.autoLogin
                  })
                  .end(function(err, res) {
                      if (err) {
                          console.log(err.message);
                          throw err;
                      }
                      expect(res.status).toBe(200);
                      expect(res.body.email).toEqual(savedPerson.email);
                      done();
                  });
          });

          it("Should throw an error if no email and autoLogin are supplied", function(done) {
              request(config.url.address)
                  .put("/api/auth")
                  .send({})
                  .end(function(err, res) {
                      if (err) {
                          console.log(err.message);
                          throw err;
                      }
                      expect(res.status).toBe(500);
                      expect(res.body.message).toBe("No email or auto login supplied");
                      done();
                  });
          });

          it("Should throw an error if the autoLogin is incorrect", function(done) {
              request(config.url.address)
                  .put("/api/auth")
                  .send({
                      email: "pants",
                      autoLogin: "pants2"
                  })
                  .end(function(err, res) {
                      if (err) {
                          console.log(err.message);
                          throw err;
                      }
                      expect(res.status).toBe(500);
                      expect(res.body.message).toBe("Auto login is incorrect");
                      done();
                  });
          });
      });

      describe("Allocate a person", function() {
          var people = [
              {
                  name: "test8",
                  accountId: "test22"
              },
              {
                  name: "test9",
                  accountId: "test22"
              },
              {
                  name: "test10",
                  accountId: "test22"
              },
              {
                  name: "test11",
                  accountId: "test22"
              }
          ];
          var savedPeople = null;
          var allocatedPersonName = null;

          before(function(done) {
              request(config.url.address)
                  .post("/api/person")
                  .send(people)
                  .end(function(err, res) {
                      if (err) {
                          console.log(err.message);
                          throw err;
                      } else {
                          savedPeople = res.body;
                      }
                      done();
                  });
          });

          it("Should not allocate itself", function(done) {
              request(config.url.address)
                  .post("/api/allocate/test22")
                  .send({
                      name: "test8"
                  })
                  .end(function(err, res) {
                      if (err) {
                          console.log(err.message);
                          throw err;
                      }
                      expect(res.status).toBe(200);
                      expect(res.body.name).not.toEqual("test8");
                      done();
                  });
          });

          it("Should allocate a person", function(done) {
              request(config.url.address)
                  .post("/api/allocate/test22")
                  .send({
                      name: "test8"
                  })
                  .end(function(err, res) {
                      if (err) {
                          console.log(err.message);
                          throw err;
                      }
                      expect(res.status).toBe(200);
                      expect(res.body.name).toBeDefined();
                      allocatedPersonName = res.body.name;
                      done();
                  });
          });

          it("Should unallocate an existing person", function(done) {
              //console.log(savedPeople);
              var savedPerson = null;
              savedPeople.peopleSaved.forEach(function(e) {
                  if (e.name === "test8") {
                      savedPerson = e;
                  }
              });

              request(config.url.address)
                  .put("/api/person/" + savedPerson._id)
                  .send({})
                  .end(function(err, res) {
                      if (err) {
                          console.log(err.message);
                          throw err;
                      }
                      expect(res.status).toBe(200);
                      expect(res.body.allocatedPerson).toBeNull();
                      done();
                  });
          });
      });
  });
})();
