(function() {
  /**
   * Created by levym on 22/11/13.
   */
  "use strict";

  var allocate = require("../../../util/allocate");

  describe("Allocate", function() {

      var docs = [
          { _id: "id1", name: "test1" },
          { _id: "id2", name: "test2" },
          { _id: "id3", name: "test3" },
          { _id: "id4", name: "test4" }
      ];

      it("Should not allocate itself", function() {
          var p = allocate.fromList(docs, "test1");
          expect(p.name).not.toEqual("test1");
      });

      it("Should return the allocated person if already allocated", function() {
          docs[1].allocatedPerson = { name: "test1" };
          var p = allocate.fromList(docs, "test2");
          expect(p.name).toEqual("test1");
      });

      it("Should allocate a unique person", function() {
          docs = [
              { _id: "id1", name: "test1" },
              { _id: "id2", name: "test2", allocatedPerson: { name: "test1" } },
              { _id: "id3", name: "test3", allocatedPerson: { name: "test2" } },
              { _id: "id4", name: "test4", allocatedPerson: { name: "test3" } }
          ];
          var p = allocate.fromList(docs, "test1");
          expect(p.name).toEqual("test4");
      });

      it("Should allocate a unique person", function() {
          docs = [
              { _id: "id1", name: "test1" },
              { _id: "id2", name: "test2", allocatedPerson: { name: "test1" } },
              { _id: "id3", name: "test3", allocatedPerson: { name: "test2" } },
              { _id: "id4", name: "test4" }
          ];
          var p = allocate.fromList(docs, "test4");
          expect(p.name).toEqual("test3");
      });

      it("Should allocate a unique person", function() {
          docs = [
              { _id: "id1", name: "test1", allocatedPerson: { name: "test4" } },
              { _id: "id2", name: "test2", allocatedPerson: { name: "test1" } },
              { _id: "id3", name: "test3", allocatedPerson: { name: "test2" } },
              { _id: "id4", name: "test4" }
          ];
          var p = allocate.fromList(docs, "test4");
          expect(p.name).toEqual("test3");
      });

      it("Should return null if all people are allocated", function() {
          docs = [
              { _id: "id1", name: "test1", allocatedPerson: { name: "test3" } },
              { _id: "id2", name: "test2", allocatedPerson: { name: "test1" } },
              { _id: "id3", name: "test3", allocatedPerson: { name: "test2" } },
              { _id: "id4", name: "test4" }
          ];
          var p = allocate.fromList(docs, "test4");
          expect(p).toBeNull();
      });

  });
})();
