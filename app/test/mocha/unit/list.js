(function() {
  /**
   * Created by levym on 16/11/13.
   */
  "use strict";

  var mongoose = require("mongoose"),
      async = require("async"),
      list = require("../../../models/list"),
      Config = require("../../../config");

  // create a new config instance
  var config = new Config();

  describe("Lists", function() {
      var testData = [
          {
              name: "Test1",
              dateCreated: new Date(),
              lastUpdated: new Date()
          },
          {
              name: "Test2",
              dateCreated: new Date(),
              lastUpdated: new Date()
          },
          {
              name: "Test3",
              dateCreated: new Date(),
              lastUpdated: new Date()
          }
      ];

      before(function(done) {
          // connect to db
          mongoose.connect(config.db.mongodb);
          done();
      });

      after(function(done) {
          // drop db
          mongoose.connection.db.dropDatabase();
          mongoose.disconnect();
          done();
      });

      describe("Create and update single instances", function() {
          var listObj = null;

          // save an instance
          beforeEach(function(done) {
              list.save(testData[0], function(doc) {
                  // success
                  //console.log("Setting ID:" + doc._id);
                  listObj = doc;
                  done();
              }, function(err) {
                  // fail
                  throw new Error(err.message);
              });
          });

          // cleanup
          afterEach(function(done) {
              // remove all people
              //console.log("Cleaning up people");
              list.model.remove({}, function() {
                  done();
              });
          });

          // tests
          it("Should save a new list", function() {
              expect(listObj.name).toEqual("Test1");
          });

          it("Should update an existing list", function() {
              listObj.name = "Test4a";
              //console.log(listObj);
              list.save(listObj, function(doc) {
                  expect(doc.name).toEqual("Test4a");
              }, function(err) {
                  throw new Error(err.message);
              });
          });

          it("Should not save if the required fields are absent", function() {
              var lo = {
                  shortUrl: "Test4",
                  dateCreated: new Date(),
                  lastUpdated: new Date()
              };

              list.save(lo, function(doc) {
                  // this won't get called
              }, function(err) {
                  expect(err).toBeDefined();
              });
          });
      });

      describe("Actions on instances", function() {
          beforeEach(function(done) {
              list.model.remove({}, function() {
                  done();
              });
          });

          afterEach(function(done) {
              list.model.remove({}, function() {
                  done();
              });
          });

          it("Should delete a list", function(done) {
              list.save(testData[0], function(doc) {
                  list.delete(doc._id, function() {
                      list.exists({ _id: doc._id }, function(exists) {
                          //console.log("Count: " + count);
                          expect(exists).toBeFalsy();
                          done();
                      }, function(err) {
                          throw new Error(err.message);
                      });
                  }, function(err) {
                      throw new Error(err.message);
                  });
              }, function(err) {
                  // fail
                  throw new Error(err.message);
              });
          });

          it("Should check if a list exists", function(done) {
              async.eachSeries(testData, function(td, cb) {
                  list.save(td, function(doc) {
                      cb();
                  }, function(err) {
                      console.log(err);
                  });
              }, function() {
                  list.exists({ name: "Test2" }, function(exists) {
                      //console.log("Docs: " + docs);
                      expect(exists).toBeTruthy();
                      done();
                  }, function(err) {
                      throw new Error(err.message);
                  });
              });
          });

          it("Should find a list by email", function(done) {
              async.eachSeries(testData, function(td, cb) {
                  list.save(td, function(doc) {
                      cb();
                  }, function(err) {
                      console.log(err);
                  });
              }, function() {
                  list.find({ name: "Test1" }, {}, function(docs) {
                      //console.log("Docs: " + docs);
                      expect(docs.length).toBe(1);
                      expect(docs[0].name).toEqual("Test1");
                      done();
                  }, function(err) {
                      throw new Error(err.message);
                  });
              });
          });

          it("Should get a single list by ID", function(done) {
              async.eachSeries(testData, function(td, cb) {
                  list.save(td, function(doc) {
                      cb();
                  }, function(err) {
                      console.log(err);
                  });
              }, function() {
                  list.find({}, {}, function(docs) {
                      var doc = docs[0];
                      list.get(doc._id, function(doc) {
                          expect(doc).toBeDefined();
                      }, function(err) {
                          throw new Error(err.message);
                      });
                      done();
                  }, function(err) {
                      throw new Error(err.message);
                  });
              });
          });

      });

  });
})();
