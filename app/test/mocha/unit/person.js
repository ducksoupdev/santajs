(function() {
  /**
   * Created by levym on 16/11/13.
   */
  "use strict";

  var mongoose = require("mongoose"),
      async = require("async"),
      person = require("../../../models/person"),
      Config = require("../../../config");

  // create a new config instance
  var config = new Config();

  describe("Persons", function() {
      var testData = [
          {
              name: "Test1",
              email: "Test1",
              password: "Test1",
              dateCreated: new Date(),
              lastUpdated: new Date()
          },
          {
              name: "Test2",
              dateCreated: new Date(),
              lastUpdated: new Date()
          },
          {
              name: "Test3",
              dateCreated: new Date(),
              lastUpdated: new Date()
          }
      ];

      before(function(done) {
          // connect to db
          mongoose.connect(config.db.mongodb);
          done();
      });

      after(function(done) {
          // drop db
          mongoose.connection.db.dropDatabase();
          mongoose.disconnect();
          done();
      });

      describe("Create and update single instances", function() {
          var personObj = null;

          // save an instance
          beforeEach(function(done) {
              person.save(testData[0], function(doc) {
                  // success
                  //console.log("Setting ID:" + doc._id);
                  personObj = doc;
                  done();
              }, function(err) {
                  // fail
                  throw new Error(err.message);
              });
          });

          // cleanup
          afterEach(function(done) {
              // remove all people
              //console.log("Cleaning up people");
              person.model.remove({}, function() {
                  done();
              });
          });

          // tests
          it("Should save a new person", function() {
              expect(personObj.email).toEqual("Test1");
          });

          it("Should update an existing person", function() {
              personObj.email = "Test4";
              //console.log(personObj);
              person.save(personObj, function(doc) {
                  expect(doc.email).toEqual("Test4");
              }, function(err) {
                  throw new Error(err.message);
              });
          });

          it("Should not save if the required fields are absent", function() {
              var lo = {
                  email: "Test4@test.com",
                  password: "Test4",
                  dateCreated: new Date(),
                  lastUpdated: new Date()
              };

              person.save(lo, function(doc) {
                  // this won't get called
              }, function(err) {
                  expect(err).toBeDefined();
              });
          });
      });

      describe("Actions on instances", function() {
          beforeEach(function(done) {
              person.model.remove({}, function() {
                  done();
              });
          });

          afterEach(function(done) {
              person.model.remove({}, function() {
                  done();
              });
          });

          it("Should delete a person", function(done) {
              person.save(testData[0], function(doc) {
                  person.delete(doc._id, function() {
                      person.exists({ _id: doc._id }, function(exists) {
                          //console.log("Count: " + count);
                          expect(exists).toBeFalsy();
                          done();
                      }, function(err) {
                          throw new Error(err.message);
                      });
                  }, function(err) {
                      throw new Error(err.message);
                  });
              }, function(err) {
                  // fail
                  throw new Error(err.message);
              });
          });

          it("Should check if a person exists", function(done) {
              async.eachSeries(testData, function(td, cb) {
                  person.save(td, function(doc) {
                      cb();
                  }, function(err) {
                      console.log(err);
                  });
              }, function() {
                  person.exists({ name: "Test2" }, function(exists) {
                      //console.log("Docs: " + docs);
                      expect(exists).toBeTruthy();
                      done();
                  }, function(err) {
                      throw new Error(err.message);
                  });
              });
          });

          it("Should find a person by email", function(done) {
              async.eachSeries(testData, function(td, cb) {
                  person.save(td, function(doc) {
                      cb();
                  }, function(err) {
                      console.log(err);
                  });
              }, function() {
                  person.find({ email: "Test1" }, {}, function(docs) {
                      //console.log("Docs: " + docs);
                      expect(docs.length).toBe(1);
                      expect(docs[0].email).toEqual("Test1");
                      done();
                  }, function(err) {
                      throw new Error(err.message);
                  });
              });
          });

          it("Should get a single person by ID", function(done) {
              async.eachSeries(testData, function(td, cb) {
                  person.save(td, function(doc) {
                      cb();
                  }, function(err) {
                      console.log(err);
                  });
              }, function() {
                  person.find({}, {}, function(docs) {
                      var doc = docs[0];
                      person.get(doc._id, function(doc) {
                          expect(doc).toBeDefined();
                      }, function(err) {
                          throw new Error(err.message);
                      });
                      done();
                  }, function(err) {
                      throw new Error(err.message);
                  });
              });
          });

      });

  });
})();
