(function() {
  /**
   * Created by levym on 16/11/13.
   */
  "use strict";

  var mongoose = require("mongoose"),
      async = require("async"),
      account = require("../../../models/account"),
      base64 = require("../../../util/base64"),
      Config = require("../../../config");

  // create a new config instance
  var config = new Config();

  describe("Accounts", function() {
      var testData = [
          {
              name: "Test1",
              dateCreated: new Date(),
              lastUpdated: new Date()
          },
          {
              name: "Test2",
              dateCreated: new Date(),
              lastUpdated: new Date()
          }
      ];

      before(function(done) {
          // connect to db
          mongoose.connect(config.db.mongodb);
          done();
      });

      after(function(done) {
          // drop db
          mongoose.connection.db.dropDatabase();
          mongoose.disconnect();
          done();
      });

      describe("Create and update single instances", function() {
          var accountObj = null;

          // save an instance
          beforeEach(function(done) {
              account.save(testData[0], function(doc) {
                  // success
                  //console.log("Setting ID:" + doc._id);
                  accountObj = doc;
                  done();
              }, function(err) {
                  // fail
                  throw new Error(err.message);
              });
          });

          // cleanup
          afterEach(function(done) {
              // remove all accounts
              //console.log("Cleaning up accounts");
              account.model.remove({}, function() {
                  done();
              });
          });

          // tests
          it("Should save a new account", function() {
              expect(accountObj.name).toEqual("Test1");
          });

          it("Should update an existing account", function() {
              accountObj.name = "Test4";
              //console.log(accountObj);
              account.save(accountObj, function(doc) {
                  expect(doc.name).toEqual("Test4");
              }, function(err) {
                  throw new Error(err.message);
              });
          });

          it("Should not save if the required fields are absent", function() {
              var lo = {
                  name: "Test4@test.com",
                  dateCreated: new Date(),
                  lastUpdated: new Date()
              };

              account.save(lo, function(doc) {
                  // this won't get called
              }, function(err) {
                  expect(err).toBeDefined();
              });
          });

          it("Should create a base64 ID of the current timestamp for the shortUrl", function() {
              var ts = new Date().getTime();
              ts = 1384770804801;
              var b = base64.fromNumber(ts);
              expect(b).toEqual("K9gnPn1");
          });
      });

      describe("Actions on instances", function() {
          beforeEach(function(done) {
              account.model.remove({}, function() {
                  done();
              });
          });

          afterEach(function(done) {
              account.model.remove({}, function() {
                  done();
              });
          });

          it("Should delete a account", function(done) {
              account.save(testData[0], function(doc) {
                  account.delete(doc._id, function() {
                      account.exists({ _id: doc._id }, function(exists) {
                          //console.log("Count: " + count);
                          expect(exists).toBeFalsy();
                          done();
                      }, function(err) {
                          throw new Error(err.message);
                      });
                  }, function(err) {
                      throw new Error(err.message);
                  });
              }, function(err) {
                  // fail
                  throw new Error(err.message);
              });
          });

          it("Should check if a account exists", function(done) {
              async.eachSeries(testData, function(td, cb) {
                  account.save(td, function(doc) {
                      cb();
                  }, function(err) {
                      console.log(err);
                  });
              }, function() {
                  account.exists({ name: "Test2" }, function(exists) {
                      //console.log("Docs: " + docs);
                      expect(exists).toBeTruthy();
                      done();
                  }, function(err) {
                      throw new Error(err.message);
                  });
              });
          });

          it("Should find a account by name", function(done) {
              async.eachSeries(testData, function(td, cb) {
                  account.save(td, function(doc) {
                      cb();
                  }, function(err) {
                      console.log(err);
                  });
              }, function() {
                  account.find({ name: "Test2" }, {}, function(docs) {
                      //console.log("Docs: " + docs);
                      expect(docs.length).toBe(1);
                      expect(docs[0].name).toEqual("Test2");
                      done();
                  }, function(err) {
                      throw new Error(err.message);
                  });
              });
          });

          it("Should get a single account by ID", function(done) {
              async.eachSeries(testData, function(td, cb) {
                  account.save(td, function(doc) {
                      cb();
                  }, function(err) {
                      console.log(err);
                  });
              }, function() {
                  account.find({}, {}, function(docs) {
                      var doc = docs[0];
                      account.get(doc._id, function(doc) {
                          expect(doc).toBeDefined();
                      }, function(err) {
                          throw new Error(err.message);
                      });
                      done();
                  }, function(err) {
                      throw new Error(err.message);
                  });
              });
          });

      });

  });
})();
