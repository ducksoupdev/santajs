(function() {
  /**
   * Created by levym on 22/11/13.
   */
  "use strict";

  var base64 = require("../../../util/base64");

  describe("Base64", function() {
      it("Should create a base64 of the current timestamp", function() {
          var ts = 1384770804801;
          var b = base64.fromNumber(ts);
          expect(b).toEqual("K9gnPn1");
      });

      it("Should get a timestamp from a base64", function() {
          var ts = 1384770804801;
          var b = base64.fromNumber(ts);
          var tsr = base64.toNumber(b);
          expect(tsr).toEqual(ts);
      });

      it("Should throw an error for non-numbers", function() {
          expect(function() { base64.fromNumber("pants"); }).toThrow("The input is not valid");
      });

      it("Should throw an error for negative numbers", function() {
          expect(function() { base64.fromNumber(-10); }).toThrow("Can't represent negative numbers now");
      });
  });
})();
